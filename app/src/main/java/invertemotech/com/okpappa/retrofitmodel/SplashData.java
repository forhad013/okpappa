
package invertemotech.com.okpappa.retrofitmodel;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

@Parcel
public class SplashData {

    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories = null;
    @SerializedName("banners")
    @Expose
    private ArrayList<Banner> banners = null;
    @SerializedName("exclusive_products")
    @Expose
    private ArrayList<ExclusiveProduct> exclusiveProducts = null;
    @SerializedName("todays_trending")
    @Expose
    private ArrayList<TodaysTrending> todaysTrending = null;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public ArrayList<Banner> getBanners() {
        return banners;
    }

    public void setBanners(ArrayList<Banner> banners) {
        this.banners = banners;
    }

    public ArrayList<ExclusiveProduct> getExclusiveProducts() {
        return exclusiveProducts;
    }

    public void setExclusiveProducts(ArrayList<ExclusiveProduct> exclusiveProducts) {
        this.exclusiveProducts = exclusiveProducts;
    }

    public ArrayList<TodaysTrending> getTodaysTrending() {
        return todaysTrending;
    }

    public void setTodaysTrending(ArrayList<TodaysTrending> todaysTrending) {
        this.todaysTrending = todaysTrending;
    }

}

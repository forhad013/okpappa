
package invertemotech.com.okpappa.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.parceler.Parcel;

@Parcel
public class Banner {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("shorttitle")
    @Expose
    private String shorttitle;
    @SerializedName("banglashorttitle")
    @Expose
    private String banglashorttitle;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("banglatitle")
    @Expose
    private String banglatitle;
    @SerializedName("shorttext1")
    @Expose
    private String shorttext1;
    @SerializedName("banglashorttext1")
    @Expose
    private String banglashorttext1;
    @SerializedName("shorttext2")
    @Expose
    private String shorttext2;
    @SerializedName("banglashorttext2")
    @Expose
    private String banglashorttext2;
    @SerializedName("link1text")
    @Expose
    private String link1text;
    @SerializedName("banglalink1text")
    @Expose
    private String banglalink1text;
    @SerializedName("link1")
    @Expose
    private String link1;
    @SerializedName("banglalink1")
    @Expose
    private String banglalink1;
    @SerializedName("link2text")
    @Expose
    private String link2text;
    @SerializedName("banglalink2text")
    @Expose
    private String banglalink2text;
    @SerializedName("link2")
    @Expose
    private String link2;
    @SerializedName("banglalink2")
    @Expose
    private String banglalink2;
    @SerializedName("imageurl")
    @Expose
    private String imageurl;
    @SerializedName("imagealttext")
    @Expose
    private String imagealttext;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("sortorder")
    @Expose
    private Integer sortorder;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShorttitle() {
        return shorttitle;
    }

    public void setShorttitle(String shorttitle) {
        this.shorttitle = shorttitle;
    }

    public String getBanglashorttitle() {
        return banglashorttitle;
    }

    public void setBanglashorttitle(String banglashorttitle) {
        this.banglashorttitle = banglashorttitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanglatitle() {
        return banglatitle;
    }

    public void setBanglatitle(String banglatitle) {
        this.banglatitle = banglatitle;
    }

    public String getShorttext1() {
        return shorttext1;
    }

    public void setShorttext1(String shorttext1) {
        this.shorttext1 = shorttext1;
    }

    public String getBanglashorttext1() {
        return banglashorttext1;
    }

    public void setBanglashorttext1(String banglashorttext1) {
        this.banglashorttext1 = banglashorttext1;
    }

    public String getShorttext2() {
        return shorttext2;
    }

    public void setShorttext2(String shorttext2) {
        this.shorttext2 = shorttext2;
    }

    public String getBanglashorttext2() {
        return banglashorttext2;
    }

    public void setBanglashorttext2(String banglashorttext2) {
        this.banglashorttext2 = banglashorttext2;
    }

    public String getLink1text() {
        return link1text;
    }

    public void setLink1text(String link1text) {
        this.link1text = link1text;
    }

    public String getBanglalink1text() {
        return banglalink1text;
    }

    public void setBanglalink1text(String banglalink1text) {
        this.banglalink1text = banglalink1text;
    }

    public String getLink1() {
        return link1;
    }

    public void setLink1(String link1) {
        this.link1 = link1;
    }

    public String getBanglalink1() {
        return banglalink1;
    }

    public void setBanglalink1(String banglalink1) {
        this.banglalink1 = banglalink1;
    }

    public String getLink2text() {
        return link2text;
    }

    public void setLink2text(String link2text) {
        this.link2text = link2text;
    }

    public String getBanglalink2text() {
        return banglalink2text;
    }

    public void setBanglalink2text(String banglalink2text) {
        this.banglalink2text = banglalink2text;
    }

    public String getLink2() {
        return link2;
    }

    public void setLink2(String link2) {
        this.link2 = link2;
    }

    public String getBanglalink2() {
        return banglalink2;
    }

    public void setBanglalink2(String banglalink2) {
        this.banglalink2 = banglalink2;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getImagealttext() {
        return imagealttext;
    }

    public void setImagealttext(String imagealttext) {
        this.imagealttext = imagealttext;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSortorder() {
        return sortorder;
    }

    public void setSortorder(Integer sortorder) {
        this.sortorder = sortorder;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}

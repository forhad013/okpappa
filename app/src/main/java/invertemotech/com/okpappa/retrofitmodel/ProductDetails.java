
package invertemotech.com.okpappa.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("categoryid")
    @Expose
    private Integer categoryid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("banglatitle")
    @Expose
    private String banglatitle;
    @SerializedName("brandid")
    @Expose
    private Integer brandid;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("shorttext")
    @Expose
    private String shorttext;
    @SerializedName("banglashorttext")
    @Expose
    private String banglashorttext;
    @SerializedName("largetext")
    @Expose
    private String largetext;
    @SerializedName("banglalargetext")
    @Expose
    private String banglalargetext;
    @SerializedName("specification")
    @Expose
    private String specification;
    @SerializedName("banglaspecification")
    @Expose
    private String banglaspecification;
    @SerializedName("mainimage")
    @Expose
    private String mainimage;
    @SerializedName("mainimagealttext")
    @Expose
    private String mainimagealttext;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("oldprice")
    @Expose
    private Integer oldprice;
    @SerializedName("currentprice")
    @Expose
    private Integer currentprice;
    @SerializedName("popular")
    @Expose
    private Integer popular;
    @SerializedName("newarrival")
    @Expose
    private Integer newarrival;
    @SerializedName("featured")
    @Expose
    private Integer featured;
    @SerializedName("todaystrending")
    @Expose
    private Integer todaystrending;
    @SerializedName("exclusive")
    @Expose
    private Integer exclusive;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("sortorder")
    @Expose
    private Integer sortorder;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanglatitle() {
        return banglatitle;
    }

    public void setBanglatitle(String banglatitle) {
        this.banglatitle = banglatitle;
    }

    public Integer getBrandid() {
        return brandid;
    }

    public void setBrandid(Integer brandid) {
        this.brandid = brandid;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getShorttext() {
        return shorttext;
    }

    public void setShorttext(String shorttext) {
        this.shorttext = shorttext;
    }

    public String getBanglashorttext() {
        return banglashorttext;
    }

    public void setBanglashorttext(String banglashorttext) {
        this.banglashorttext = banglashorttext;
    }

    public String getLargetext() {
        return largetext;
    }

    public void setLargetext(String largetext) {
        this.largetext = largetext;
    }

    public String getBanglalargetext() {
        return banglalargetext;
    }

    public void setBanglalargetext(String banglalargetext) {
        this.banglalargetext = banglalargetext;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getBanglaspecification() {
        return banglaspecification;
    }

    public void setBanglaspecification(String banglaspecification) {
        this.banglaspecification = banglaspecification;
    }

    public String getMainimage() {
        return mainimage;
    }

    public void setMainimage(String mainimage) {
        this.mainimage = mainimage;
    }

    public String getMainimagealttext() {
        return mainimagealttext;
    }

    public void setMainimagealttext(String mainimagealttext) {
        this.mainimagealttext = mainimagealttext;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Integer getOldprice() {
        return oldprice;
    }

    public void setOldprice(Integer oldprice) {
        this.oldprice = oldprice;
    }

    public Integer getCurrentprice() {
        return currentprice;
    }

    public void setCurrentprice(Integer currentprice) {
        this.currentprice = currentprice;
    }

    public Integer getPopular() {
        return popular;
    }

    public void setPopular(Integer popular) {
        this.popular = popular;
    }

    public Integer getNewarrival() {
        return newarrival;
    }

    public void setNewarrival(Integer newarrival) {
        this.newarrival = newarrival;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Integer getTodaystrending() {
        return todaystrending;
    }

    public void setTodaystrending(Integer todaystrending) {
        this.todaystrending = todaystrending;
    }

    public Integer getExclusive() {
        return exclusive;
    }

    public void setExclusive(Integer exclusive) {
        this.exclusive = exclusive;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSortorder() {
        return sortorder;
    }

    public void setSortorder(Integer sortorder) {
        this.sortorder = sortorder;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}

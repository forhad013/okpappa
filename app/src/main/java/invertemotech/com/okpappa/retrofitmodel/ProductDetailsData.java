
package invertemotech.com.okpappa.retrofitmodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsData {

    @SerializedName("product")
    @Expose
    private ProductDetails product;
    @SerializedName("similar")
    @Expose
    private ArrayList<SimilarProducts> similar = null;

    public ProductDetails getProduct() {
        return product;
    }

    public void setProduct(ProductDetails product) {
        this.product = product;
    }

    public ArrayList<SimilarProducts> getSimilar() {
        return similar;
    }

    public void setSimilar(ArrayList<SimilarProducts> similar) {
        this.similar = similar;
    }

}

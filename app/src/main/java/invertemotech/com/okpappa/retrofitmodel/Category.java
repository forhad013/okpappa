
package invertemotech.com.okpappa.retrofitmodel;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.parceler.Parcel;

@Parcel
public class Category {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("banglatitle")
    @Expose
    private String banglatitle;
    @SerializedName("shorttext")
    @Expose
    private String shorttext;
    @SerializedName("banglashorttext")
    @Expose
    private String banglashorttext;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("imagealttext")
    @Expose
    private String imagealttext;
    @SerializedName("parent")
    @Expose
    private String parent;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("sortorder")
    @Expose
    private Integer sortorder;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("subcategories")
    @Expose
    private ArrayList<Subcategory> subcategories = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanglatitle() {
        return banglatitle;
    }

    public void setBanglatitle(String banglatitle) {
        this.banglatitle = banglatitle;
    }

    public String getShorttext() {
        return shorttext;
    }

    public void setShorttext(String shorttext) {
        this.shorttext = shorttext;
    }

    public String getBanglashorttext() {
        return banglashorttext;
    }

    public void setBanglashorttext(String banglashorttext) {
        this.banglashorttext = banglashorttext;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImagealttext() {
        return imagealttext;
    }

    public void setImagealttext(String imagealttext) {
        this.imagealttext = imagealttext;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSortorder() {
        return sortorder;
    }

    public void setSortorder(Integer sortorder) {
        this.sortorder = sortorder;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ArrayList<Subcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(ArrayList<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

}

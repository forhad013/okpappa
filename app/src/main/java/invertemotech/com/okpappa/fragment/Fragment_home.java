package invertemotech.com.okpappa.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.activity.LoginActivity;
import invertemotech.com.okpappa.activity.MainActivity;
import invertemotech.com.okpappa.activity.ProductDetailsActivity;
import invertemotech.com.okpappa.activity.ProductListActivity;
import invertemotech.com.okpappa.activity.SignUpActivity;
import invertemotech.com.okpappa.adapter.CategoryListAdapter;
import invertemotech.com.okpappa.adapter.ExclusiveProductListAdapter;
import invertemotech.com.okpappa.adapter.ProductListAdapter;
import invertemotech.com.okpappa.adapter.SlidingImage_Adapter;
import invertemotech.com.okpappa.adapter.TrendingProductListAdapter;
import invertemotech.com.okpappa.model.CategoryModel;
import invertemotech.com.okpappa.model.ProductsModel;
import invertemotech.com.okpappa.model.SubCategoryModel;
import invertemotech.com.okpappa.retrofitmodel.Banner;
import invertemotech.com.okpappa.retrofitmodel.Category;
import invertemotech.com.okpappa.retrofitmodel.ExclusiveProduct;
import invertemotech.com.okpappa.retrofitmodel.TodaysTrending;
import invertemotech.com.okpappa.util.ConnectionDetector;
import invertemotech.com.okpappa.util.DB;
import invertemotech.com.okpappa.util.GridUtility;
import invertemotech.com.okpappa.util.SharePref;


public class Fragment_home extends Fragment {

    ArrayList<ExclusiveProduct> exclusiveProducts;
    ArrayList<TodaysTrending> todaysTrendingArrayList;
    ExclusiveProductListAdapter exclusiveProdcutAdapter;
    TrendingProductListAdapter trendingProductListAdapter;

    RecyclerView categoryList;
    ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();

    ArrayList<String> ImagesArray;
    ImageButton post;
    SharePref sharePref;
    int NUM_PAGES;

    GridView exclusiveProductList,todaysTrending;
    int currentPage=0;

    ViewPager mPager;
    DB db;

    ConnectionDetector cd;
    boolean isInternetON;
    Context context;
    CirclePageIndicator indicator;
    Button signIn,signUp;

    ArrayList<Category> categoryArrayList;
    ArrayList<Banner> bannerArrayList;

    Button viewExclusive,viewTrending;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        View view = inflater.inflate(R.layout.fragment_home, container,
                false);

        mPager = (ViewPager) view.findViewById(R.id.pager);
        context = getActivity();
          indicator = (CirclePageIndicator)
                  view.findViewById(R.id.indicator);

        cd = new ConnectionDetector(context);
        isInternetON = cd.isConnectingToInternet();

        signIn = (Button) view.findViewById(R.id.signIn);

        signUp = (Button) view.findViewById(R.id.signUp);
        viewExclusive = (Button) view.findViewById(R.id.viewExclusive);
        viewTrending = (Button) view.findViewById(R.id.viewTrending);

        db= new DB(getActivity());


        categoryArrayList = new ArrayList<>();
        bannerArrayList = new ArrayList<>();
        exclusiveProducts = new ArrayList<>();
        todaysTrendingArrayList = new ArrayList<>();




        categoryArrayList =  ((MainActivity)getActivity()).splashData.getCategories();
        bannerArrayList =  ((MainActivity)getActivity()).splashData.getBanners();
        exclusiveProducts =  ((MainActivity)getActivity()).splashData.getExclusiveProducts();
        todaysTrendingArrayList =  ((MainActivity)getActivity()).splashData.getTodaysTrending();




        sharePref = new SharePref(context);
        exclusiveProductList = (GridView) view.findViewById(R.id.exclusiveList);
        todaysTrending = (GridView) view.findViewById(R.id.trendingList);
        categoryList = (RecyclerView) view.findViewById(R.id.categoryList);

        init();


        setList();

//        exclusiveProductList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent it = new Intent(context,ProductDetailsActivity.class);
//                startActivity(it);
//            }
//        });

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(context,LoginActivity.class);
                startActivity(it);
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(context,SignUpActivity.class);
                startActivity(it);
            }
        });





        viewTrending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,ProductListActivity.class);
                intent.putExtra("token",  "trending");
                intent.putExtra("subcategoryId", "0");
                intent.putExtra("search", false);
                startActivity(intent);
            }
        });


        viewExclusive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ProductListActivity.class);
                intent.putExtra("token",  "exclusive");
                intent.putExtra("subcategoryId", "0");
                intent.putExtra("search", false);
                startActivity(intent);
            }
        });




        return view;
    }



   public void setList(){




       exclusiveProdcutAdapter = new ExclusiveProductListAdapter(context,exclusiveProducts);
       exclusiveProductList.setAdapter(exclusiveProdcutAdapter);
       GridUtility.setListViewHeightBasedOnChildren(exclusiveProductList);

       exclusiveProdcutAdapter.setCustomObjectListener(new ExclusiveProductListAdapter.onItemClickListener() {
           @Override
           public void onItemClicked(int position, String token) {
               Log.e("poaition",position+"");

               if(token.equals("view")){

                   Intent it = new Intent(getActivity(),ProductDetailsActivity.class);
                   it.putExtra("productId",exclusiveProducts.get(position).getId()+"");
                   startActivity(it);
               }else if(token.equals("wish")){

                   if(!db.checkWishlistItem(exclusiveProducts.get(position).getId()+"")){
                       db.addProductInWishlist(exclusiveProducts.get(position).getId()+"",
                               exclusiveProducts.get(position).getTitle(),
                               exclusiveProducts.get(position).getBrandid()+"",
                               exclusiveProducts.get(position).getCategoryid()+"",
                               exclusiveProducts.get(position).getCurrentprice()+"",
                               exclusiveProducts.get(position).getOldprice()+"","5",
                               exclusiveProducts.get(position).getMainimage(),
                               "1");

                       Toast.makeText(context,"Added to "+token,Toast.LENGTH_LONG).show();

                   }
                   else{

                       db.removeFromWishlist(exclusiveProducts.get(position).getId()+"");
                   }


               }
               else if(token.equals("cart")){
                   db.addProductInCart(exclusiveProducts.get(position).getId()+"",
                           exclusiveProducts.get(position).getTitle(),
                           exclusiveProducts.get(position).getBrandid()+"",
                           exclusiveProducts.get(position).getCategoryid()+"",
                           exclusiveProducts.get(position).getCurrentprice()+"",
                           exclusiveProducts.get(position).getOldprice()+"","5",
                           exclusiveProducts.get(position).getMainimage(),
                           "1");

                   ((MainActivity)getActivity()).setCartCounter();

                   Toast.makeText(context,"Added to "+token,Toast.LENGTH_LONG).show();


               }


           }
       });



       trendingProductListAdapter = new TrendingProductListAdapter(context,todaysTrendingArrayList);
       todaysTrending.setAdapter(trendingProductListAdapter);
       GridUtility.setListViewHeightBasedOnChildren(todaysTrending);

       trendingProductListAdapter.setCustomObjectListener(new TrendingProductListAdapter.onItemClickListener() {
           @Override
           public void onItemClicked(int position, String token) {
               if(token.equals("view")){

                   Intent it = new Intent(getActivity(),ProductDetailsActivity.class);
                   it.putExtra("productId",todaysTrendingArrayList.get(position).getId()+"");
                   startActivity(it);
               }
               else if(token.equals("wish")){

                   if(!db.checkWishlistItem(todaysTrendingArrayList.get(position).getId()+"")){
                       db.addProductInWishlist(todaysTrendingArrayList.get(position).getId()+"",
                               todaysTrendingArrayList.get(position).getTitle(),
                               todaysTrendingArrayList.get(position).getBrandid()+"",
                               todaysTrendingArrayList.get(position).getCategoryid()+"",
                               todaysTrendingArrayList.get(position).getCurrentprice()+"",
                               todaysTrendingArrayList.get(position).getOldprice()+"","5",
                               todaysTrendingArrayList.get(position).getMainimage(),
                               "1");

                       Toast.makeText(context,"Added to "+token,Toast.LENGTH_LONG).show();

                   }else{

                       db.removeFromWishlist(todaysTrendingArrayList.get(position).getId()+"");
                   }



               } else if(token.equals("cart")){
                   db.addProductInCart(todaysTrendingArrayList.get(position).getId()+"",
                           todaysTrendingArrayList.get(position).getTitle(),
                           todaysTrendingArrayList.get(position).getBrandid()+"",
                           todaysTrendingArrayList.get(position).getCategoryid()+"",
                           todaysTrendingArrayList.get(position).getCurrentprice()+"",
                           todaysTrendingArrayList.get(position).getOldprice()+"","5",
                           todaysTrendingArrayList.get(position).getMainimage(),
                           "1");

                   ((MainActivity)getActivity()).setCartCounter();

                   Toast.makeText(context,"Added to "+token,Toast.LENGTH_LONG).show();


               }
           }
       });





       CategoryListAdapter categoryAdapter = new CategoryListAdapter(categoryArrayList,context);
       categoryList.setAdapter( categoryAdapter);

       LinearLayoutManager layoutManager
               = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);


       categoryList.setLayoutManager(layoutManager);

   }

    @Override
        public void onResume() {

        super.onResume();
    }






    public void setdummy(){

    }

    private void init() {

        ImagesArray = new ArrayList<>();

        for(int i=0;i<bannerArrayList.size();i++){

            ImagesArray.add(bannerArrayList.get(i).getImageurl());
        }





        mPager.setAdapter(new SlidingImage_Adapter(getActivity(),ImagesArray));




        indicator.setViewPager(mPager);
        indicator.setSnap(true);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =ImagesArray.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

}
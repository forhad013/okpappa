package invertemotech.com.okpappa.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;





import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import invertemotech.com.okpappa.model.CartProductModel;

/**
 * Created by Mac on 11/12/15.
 */
public class DB extends SQLiteAssetHelper {

   // static Database instance = null;
    static SQLiteDatabase database = null;
    SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm a");
    SharePref sharePref;

    public static final String DATABASE_NAME = "okPappa.db";
    static final int DATABASE_VERSION = 1;





    public static final String WISH_TABLE = "wishlist";





    public static final String CART_TABLE = "cart";
    public static final String CART_COLUMN_ID = "id";
    public static final String CART_USER_ID = "user_id";
    public static final String CART_PRODCUT_ID = "pro_id";
    public static final String CART_PRODUCT_TITLE = "pro_name";
    public static final String CART_PRODUCT_IMAGE = "pro_image";
    public static final String CART_PRICE_NOW = "pro_price_now";
    public static final String CART_PRICE_PREVIOUS = "pro_price_previous";
    public static final String CART_QUANTITY = "quantity";
    public static final String CART_RATING = "rating";
    public static final String CART_BRAND = "brand";
    public static final String CART_CATEGORY = "category";





    public DB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        sharePref = new SharePref(context);


    }


    public   boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }





    public void addProductInCart(String proID, String proName, String proBrand, String proCategory, String proPriceNow, String proPricePrevious, String proRating, String proImage, String proQuantity ) {

        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();

        String userId = "0";


        String retrieve = "SELECT * FROM "+CART_TABLE+ " where "+CART_PRODCUT_ID +" = "+proID + " and "+CART_USER_ID +" = "+userId ;

        c = db.rawQuery(retrieve, null);


        if (c.getCount() != 0) {

            if (c.moveToFirst()) {
                do {


                    int quantity= c.getInt(c.getColumnIndex(CART_QUANTITY));

                    int id = c.getInt(c.getColumnIndex(CART_COLUMN_ID));

                    int x = quantity+Integer.parseInt(proQuantity);

                    ContentValues cv = new ContentValues();

                    cv.put(CART_QUANTITY, x);
                    db.update(CART_TABLE, cv, "id =?", new String[]{id+""});
                } while (c.moveToNext());
            }
        }else {
            ContentValues cv = new ContentValues();
            cv.put(CART_PRODCUT_ID, proID);
            cv.put(CART_USER_ID, userId);
            cv.put(CART_PRODUCT_TITLE, proName);
            cv.put(CART_PRODUCT_IMAGE, proImage);

            cv.put(CART_PRICE_NOW, proPriceNow);
            cv.put(CART_PRICE_PREVIOUS, proPricePrevious);
            cv.put(CART_QUANTITY, proQuantity);
            cv.put(CART_RATING, proRating);
            cv.put(CART_BRAND, proBrand);
            cv.put(CART_CATEGORY, proCategory);
            db.insert(CART_TABLE, null, cv);
        }

        db.close();
    }



    public String getCartCount(){
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();

        String userId = "0";


        String retrieve = "SELECT * FROM "+CART_TABLE+ " where " +CART_USER_ID +" = "+userId ;

        c = db.rawQuery(retrieve, null);


        return  c.getCount()+"";

    }


    public ArrayList getCartProducts() {

        String searchDate = "";


        String userId = "0";
        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "SELECT * FROM "+CART_TABLE+ " where " +CART_USER_ID +" = "+userId ;


        Log.e("check", retrieve);


        ArrayList<CartProductModel> cartProductModelArrayList;
        cartProductModelArrayList = new ArrayList<>();
        Cursor c;
        c = db.rawQuery(retrieve, null);

        Log.e("count", c+"");

        String proID,proName, proBrand,proCategory, proPriceNow, proPricePrevious,
        proRating, proImage,proQuantity;


            if (c.getCount() != 0) {


            if (c.moveToFirst()) {
                do {

                    String id= c.getString(c.getColumnIndex(CART_COLUMN_ID));
                      proID= c.getString(c.getColumnIndex(CART_PRODCUT_ID));

                    proName= c.getString(c.getColumnIndex(CART_PRODUCT_TITLE));
                    proImage = c.getString(c.getColumnIndex(CART_PRODUCT_IMAGE));

                    proPriceNow = c.getString(c.getColumnIndex(CART_PRICE_NOW));
                    proPricePrevious = c.getString(c.getColumnIndex(CART_PRICE_PREVIOUS));


                    proQuantity= c.getString(c.getColumnIndex(CART_QUANTITY));
                    proRating= c.getString(c.getColumnIndex(CART_RATING));
                    proBrand= c.getString(c.getColumnIndex(CART_BRAND));
                    proCategory = c.getString(c.getColumnIndex(CART_CATEGORY));


                    CartProductModel cartProductModel = new CartProductModel(proID,proName, proBrand,proCategory, proPriceNow, proPricePrevious,
                            proRating, proImage,proQuantity );
                    cartProductModelArrayList.add(cartProductModel);



                } while (c.moveToNext());

            }

        }



        db.close();


        return cartProductModelArrayList;

    }

    public void updateQuantity(String productId,String qunatity){
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();

        String userId = "0";


        String retrieve = "SELECT * FROM "+CART_TABLE+ " where "+CART_PRODCUT_ID +" = "+productId;

        c = db.rawQuery(retrieve, null);


        if (c.getCount() != 0) {

            if (c.moveToFirst()) {
                do {


                    int quantity= c.getInt(c.getColumnIndex(CART_QUANTITY));

                    int id = c.getInt(c.getColumnIndex(CART_COLUMN_ID));



                    ContentValues cv = new ContentValues();

                    cv.put(CART_QUANTITY, qunatity);
                    db.update(CART_TABLE, cv, "id =?", new String[]{id+""});
                } while (c.moveToNext());
            }
        }

    }

    public void removeFromCart(String productId){
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();

        String userId = "0";


        db .delete(CART_TABLE,
                CART_USER_ID + "=? AND " + CART_PRODCUT_ID + "=?   " ,
                new String[] {userId, productId});

    }





    public void addProductInWishlist(String proID, String proName, String proBrand, String proCategory, String proPriceNow, String proPricePrevious, String proRating, String proImage, String proQuantity ) {

        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();

        String userId = "0";



            ContentValues cv = new ContentValues();
            cv.put(CART_PRODCUT_ID, proID);
            cv.put(CART_USER_ID, userId);
            cv.put(CART_PRODUCT_TITLE, proName);
            cv.put(CART_PRODUCT_IMAGE, proImage);

            cv.put(CART_PRICE_NOW, proPriceNow);
            cv.put(CART_PRICE_PREVIOUS, proPricePrevious);
            cv.put(CART_QUANTITY, proQuantity);
            cv.put(CART_RATING, proRating);
            cv.put(CART_BRAND, proBrand);
            cv.put(CART_CATEGORY, proCategory);
            db.insert(WISH_TABLE, null, cv);


        db.close();
    }



    public boolean checkWishlistItem(String token){
        boolean status = false;

        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();
        String userId = "0";
        String retrieve = "SELECT * FROM "+WISH_TABLE+ " where "+CART_PRODCUT_ID +" = "+token + " and "+CART_USER_ID +" = "+userId ;

        c = db.rawQuery(retrieve, null);



        if (c.getCount() != 0) {

           // Log.e("staus",status+"");

            status = true;
        }

        return status;
    }


    public void removeFromWishlist(String productId){
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();

        String userId = "0";


        db .delete(WISH_TABLE,
                CART_USER_ID + "=? AND " + CART_PRODCUT_ID + "=?   " ,
                new String[] {userId, productId});

    }


    public ArrayList getWish() {

        String searchDate = "";


        String userId = "0";
        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "SELECT * FROM "+WISH_TABLE+ " where " +CART_USER_ID +" = "+userId ;




        Log.e("check", retrieve);


        ArrayList<CartProductModel> cartProductModelArrayList;
        cartProductModelArrayList = new ArrayList<>();
        Cursor c;
        c = db.rawQuery(retrieve, null);

        Log.e("count", c+"");

        String proID,proName, proBrand,proCategory, proPriceNow, proPricePrevious,
                proRating, proImage,proQuantity;


        if (c.getCount() != 0) {


            if (c.moveToFirst()) {
                do {

                    String id= c.getString(c.getColumnIndex(CART_COLUMN_ID));
                    proID= c.getString(c.getColumnIndex(CART_PRODCUT_ID));

                    proName= c.getString(c.getColumnIndex(CART_PRODUCT_TITLE));
                    proImage = c.getString(c.getColumnIndex(CART_PRODUCT_IMAGE));

                    proPriceNow = c.getString(c.getColumnIndex(CART_PRICE_NOW));
                    proPricePrevious = c.getString(c.getColumnIndex(CART_PRICE_PREVIOUS));


                    proQuantity= c.getString(c.getColumnIndex(CART_QUANTITY));
                    proRating= c.getString(c.getColumnIndex(CART_RATING));
                    proBrand= c.getString(c.getColumnIndex(CART_BRAND));
                    proCategory = c.getString(c.getColumnIndex(CART_CATEGORY));


                    CartProductModel cartProductModel = new CartProductModel(proID,proName, proBrand,proCategory, proPriceNow, proPricePrevious,
                            proRating, proImage,proQuantity );
                    cartProductModelArrayList.add(cartProductModel);



                } while (c.moveToNext());

            }

        }



        db.close();


        return cartProductModelArrayList;

    }


    public void updateQuantityWishlist(String productId,String qunatity){
        Cursor c;
        SQLiteDatabase db = this.getReadableDatabase();

        String userId = "0";


        String retrieve = "SELECT * FROM "+CART_TABLE+ " where "+CART_PRODCUT_ID +" = "+productId;

        c = db.rawQuery(retrieve, null);


        if (c.getCount() != 0) {

            if (c.moveToFirst()) {
                do {


                    int quantity= c.getInt(c.getColumnIndex(CART_QUANTITY));

                    int id = c.getInt(c.getColumnIndex(CART_COLUMN_ID));



                    ContentValues cv = new ContentValues();

                    cv.put(CART_QUANTITY, qunatity);
                    db.update(WISH_TABLE, cv, "id =?", new String[]{id+""});
                } while (c.moveToNext());
            }
        }

    }
}
package invertemotech.com.okpappa.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Smartnjazzy on 8/30/2015.
 */
public class SharePref {

    public static final String PREF = "pref";
    static public String LANGUAGE = "language";

    static public String USERID = "userID";
    static public String STOREID = "storeID";
    static public String USERNAME = "userName";
    static public String COMPANY = "company";
    static public String SESSIONTOKEN = "sessionkey";
    static public String LATITUDE = "latitude";
    static public String LONGITUDE = "longitude";
    static public String LOGEDIN = "login";
    static public String LOGEDIN_METHOD = "login_method";
    public static final String IS_LOGGED_IN = "is_logged_in";

    static public String USERPHONE = "userNumber";

    static public String SELECTED_ID = "selected_id";
    static public String USERTYPE = "userType";

    static public String OLDUSER = "oldUser";
    static public String IP = "ip";
    static public String POSITION = "position";

    static public String USERPASSWWORD = "userPassword";
    static public String USEREMAIL = "userEmail";

    static public String USERPROVINCE = "province";
    static public String GENDER = "gender";
    static public String AGE = "age";
    static public String PROFILEPIC = "picture";
    static public String TOPICS = "topics";

    static public String CURRENTSHOPID = "shopId";
    static public String THEMECOLOR = "theme";
    static public String POSTER = "poster";
    static public String UPDATEDATE = "update";
    static public String HASCOME = "hascome";
    static public String FIRSTTIME = "first";
    static public String JUSTCREATED = "just";


    static public String FOLLOWEDIT = "follow";
    static public String FOLLOWID = "followid";
    static public String FOLLOWDATA = "data";
    static public String DONT_SHOW_IMAGE = "image";




    SharedPreferences settings;


    public SharePref(Context mContext) {
        settings = mContext.getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }


    public boolean setIsLoggedIn(boolean value) {
        settings.edit().putBoolean(IS_LOGGED_IN, value).commit();
        return value;
    }

    public boolean isLoggedIn() {
        return settings.getBoolean(IS_LOGGED_IN, false);
    }
    public int getshareprefdata(String KEY) {
        return settings.getInt(KEY, 160);
    }

    public void setshareprefdata(String KEY, int value) {

        settings.edit().putInt(KEY, value).commit();
    }


    public String getshareprefdatastring(String KEY) {
        return settings.getString(KEY, "");
    }

    public void setshareprefdatastring(String KEY, String values) {

        settings.edit().putString(KEY, values).commit();
    }

    public Boolean getshareprefdataBoolean(String KEY) {
        return settings.getBoolean(KEY, false);
    }

    public void setshareprefdataBoolean(String KEY, Boolean values) {

        settings.edit().putBoolean(KEY, values).commit();
    }
}

package invertemotech.com.okpappa.retrofit;


import invertemotech.com.okpappa.retrofitmodel.AutoCompleteResponse;
import invertemotech.com.okpappa.retrofitmodel.CategoryBrandResponse;
import invertemotech.com.okpappa.retrofitmodel.ProductDetailsResponse;
import invertemotech.com.okpappa.retrofitmodel.ProductFilterResponse;
import invertemotech.com.okpappa.retrofitmodel.SplashResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Mac on 8/11/16.
 */
public interface ApiInterafce {





        //topicsList
    @GET("api/v1.2/home")
    Call<SplashResponse> getSplashData();



    @GET("api/v1.2/productsfilter/{subcategoryid}/{brandsid}/{price1}/{price2}/{sortkey}/{term}")
    Call<ProductFilterResponse> getProducts(@Path("subcategoryid") int subcategoryid,
                                       @Path("brandsid") int brandsid,
                                       @Path("price1") String price1,

                                       @Path("price2") String price2,
                                       @Path("sortkey") int sortkey,
                                            @Path("term") String term,
                                       @Query("page") String page);




    @GET("api/v1.2/getproductsbytype/{type}/{subcategoryid}/{brandsid}/{price1}/{price2}/{sortkey}/{term}")
    Call<ProductFilterResponse> getProductsByTypes(@Path("type") String type,
                                       @Path("subcategoryid") int subcategoryid,
                                       @Path("brandsid") int brandsid,
                                       @Path("price1") String price1,
                                       @Path("price2") String price2,
                                       @Path("sortkey") int sortkey,
                                       @Path("term") String term,
                                       @Query("page") String page);


    @GET("api/v1.2/categorybrands")
    Call<CategoryBrandResponse> getCategoryBrand();

    @GET("api/v1.2/productdetails/{id}")
    Call<ProductDetailsResponse> getProductDetails(@Path("id") int id);


    @GET("api/v1.2/autosuggestion/{term}")
    Call<AutoCompleteResponse> getAutoComplete(@Path("term") String term);

}

package invertemotech.com.okpappa.model;

/**
 * Created by HP on 02/08/2015.
 */
public final class SubCategoryModel {


  public String getSubCategoryName() {
    return subCategoryName;
  }

  public void setSubCategoryName(String subCategoryName) {
    this.subCategoryName = subCategoryName;
  }

  public String getSubCategoryID() {
    return subCategoryID;
  }

  public void setSubCategoryID(String subCategoryID) {
    this.subCategoryID = subCategoryID;
  }



  public SubCategoryModel(String subCategoryID, String subCategoryName) {
    this.subCategoryID = subCategoryID;
    this.subCategoryName = subCategoryName;
  }
  public String subCategoryID;
  public String subCategoryName;






}





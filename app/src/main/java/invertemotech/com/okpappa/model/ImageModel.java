package invertemotech.com.okpappa.model;

/**
 * Created by HP on 02/08/2015.
 */
public final class ImageModel {


  public ImageModel(String id, String url) {
    this.id = id;
    this.url = url;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String id;



  public String url;






}





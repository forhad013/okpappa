package invertemotech.com.okpappa.model;

/**
 * Created by HP on 02/08/2015.
 */
public final class ProductsModel {




  public String getProID() {
    return proID;
  }

  public void setProID(String proID) {
    this.proID = proID;
  }

  public String getProName() {
    return proName;
  }

  public void setProName(String proName) {
    this.proName = proName;
  }

  public String getProBrand() {
    return proBrand;
  }

  public void setProBrand(String proBrand) {
    this.proBrand = proBrand;
  }

  public String getProCategory() {
    return proCategory;
  }

  public void setProCategory(String proCategory) {
    this.proCategory = proCategory;
  }

  public String getProPriceNow() {
    return proPriceNow;
  }

  public void setProPriceNow(String proPriceNow) {
    this.proPriceNow = proPriceNow;
  }

  public String getProPricePrevious() {
    return proPricePrevious;
  }

  public void setProPricePrevious(String proPricePrevious) {
    this.proPricePrevious = proPricePrevious;
  }

  public String getProRating() {
    return proRating;
  }

  public void setProRating(String proRating) {
    this.proRating = proRating;
  }

  public String getProImage() {
    return proImage;
  }

  public void setProImage(String proImage) {
    this.proImage = proImage;
  }

  public String proName;
    public String proBrand;
    public String proCategory;
    public String proPriceNow;

  public ProductsModel(  String proID,String proName, String proBrand, String proCategory, String proPriceNow, String proPricePrevious, String proRating, String proImage) {
    this.proName = proName;
    this.proBrand = proBrand;
    this.proCategory = proCategory;
    this.proPriceNow = proPriceNow;
    this.proPricePrevious = proPricePrevious;
    this.proRating = proRating;
    this.proImage = proImage;
    this.proID = proID;
  }

  public String proPricePrevious;
    public String proRating;
    public String proImage;
  public String proID;





}





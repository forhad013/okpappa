package invertemotech.com.okpappa.model;

import java.util.ArrayList;

/**
 * Created by HP on 02/08/2015.
 */
public final class CategoryModel {


  public String getCatID() {
    return catID;
  }

  public void setCatID(String catID) {
    this.catID = catID;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public ArrayList<SubCategoryModel> getSubCategories() {
    return subCategories;
  }

  public void setSubCategories(ArrayList<SubCategoryModel> subCategories) {
    this.subCategories = subCategories;
  }

  public String catID;

  public CategoryModel(String catID, String catName, ArrayList<SubCategoryModel> subCategories) {
    this.catID = catID;
    this.catName = catName;
    this.subCategories = subCategories;
  }

  public String catName;
    public ArrayList<SubCategoryModel> subCategories;





}





package invertemotech.com.okpappa.model;

/**
 * Created by Admin on 12/15/2015.
 */
public class AutoCompleteModel {
    private String autoID, autoTitle, autoType, autoCount, autoImage,autoGroupId,autoProfile;

    public AutoCompleteModel(String autoID, String autoTitle) {
        this.autoID = autoID;
        this.autoTitle = autoTitle;

    }

    public String getAutoID() {
        return autoID;
    }

    public void setAutoID(String autoID) {
        this.autoID = autoID;
    }

    public String getAutoTitle() {
        return autoTitle;
    }

    public void setAutoTitle(String autoTitle) {
        this.autoTitle = autoTitle;
    }


}

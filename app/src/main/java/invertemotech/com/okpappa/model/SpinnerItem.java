package invertemotech.com.okpappa.model;

import java.util.ArrayList;

/**
 * Created by HP on 02/08/2015.
 */
public final class SpinnerItem {


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }

  public String title;
  public boolean isSelected;

  public SpinnerItem(String title, boolean isSelected) {
    this.title = title;
    this.isSelected = isSelected;
  }








}





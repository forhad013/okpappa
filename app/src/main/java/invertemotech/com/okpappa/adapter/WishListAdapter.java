package invertemotech.com.okpappa.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.model.ProductsModel;


public class WishListAdapter extends BaseAdapter {



	ArrayList<ProductsModel> productsModelArrayList;
	int tag;
    String token;

	onItemClickListener callback;
	private Context context;

	public WishListAdapter(Context context,
                           ArrayList<ProductsModel> productsModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.productsModelArrayList = productsModelArrayList;


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productsModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


		if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.wish_list_item, parent, false);
		}

			TextView title1 = (TextView) view.findViewById(R.id.title);
			TextView priceNow =(TextView) view.findViewById(R.id.price);

			ImageView image1 = (ImageView) view.findViewById(R.id.image);

		TextView quantity =(TextView) view.findViewById(R.id.quantity);

			RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.rel);



		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
		view.setLayoutParams(new GridView.LayoutParams(params));

//			String url =PopularProductImage.get(position);
//
//
//
//
//			Picasso.with(context).load(url).placeholder(R.drawable.loading_cart)
//					.error(R.drawable.loading_cart).into(image1);


			title1.setText(productsModelArrayList.get(position).getProName());
		priceNow.setText("TK. "+productsModelArrayList.get(position).getProPriceNow());

		 int r =R.drawable.sample_product;
		image1.setImageResource(r);



		setCustomObjectListener(callback);

		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"view");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});


		quantity.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"quantity");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});







		// Picasso.with(context).load(image.get(position)).into(imageView1);

		return view;
	}

	public interface onItemClickListener {
		public void onItemClicked(int position, String token);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		this.callback = listener;
	}



}
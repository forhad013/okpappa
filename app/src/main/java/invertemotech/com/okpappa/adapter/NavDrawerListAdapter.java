package invertemotech.com.okpappa.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.util.SharePref;


public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<String> navDrawerItems;

	private ArrayList<Integer> navDrawerItemsImages;

	public onProfilePicListener callback;

	String userID;
	SharePref sharePref;
	Typeface typeface;
	
	public NavDrawerListAdapter(Context context, ArrayList<String> navDrawerItems, ArrayList<Integer> navDrawerItemsImages){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
		this.navDrawerItemsImages = navDrawerItemsImages;
		sharePref = new SharePref(context);


		userID= sharePref.getshareprefdatastring(SharePref.USERID);
//		typeface = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd-Md.otf");

	}

	public interface onProfilePicListener {
		public void onProfilePicClicked();

	}

	public void setCustomObjectListener(onProfilePicListener callback) {
		Log.e("onCus","onCus");
		Log.e("listener",callback+" nm");
		this.callback = callback;


		Log.e("listenercallback",callback+" nm");
	}


	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}



	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {





		if (convertView == null) {
			if(position==0) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item_signin, parent, false);



			}


			else {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item, parent, false);
				ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
				TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

				//txtTitle.setTypeface(typeface);

				if(position!=0) {
					// imgIcon.setImageResource(navDrawerItemsImages.get(position));
					 txtTitle.setText(navDrawerItems.get(position));
					imgIcon.setImageResource(navDrawerItemsImages.get(position));
				}
			}
		}




//


    
        
        return convertView;
	}
	
	

}

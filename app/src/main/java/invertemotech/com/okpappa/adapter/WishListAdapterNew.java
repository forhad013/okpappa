package invertemotech.com.okpappa.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.model.CartProductModel;


public class WishListAdapterNew extends BaseAdapter {



	ArrayList<CartProductModel> productsModelArrayList;
	int tag;
    String token;

	onItemClickListener callback;
	private Context context;

	public WishListAdapterNew(Context context,
                              ArrayList<CartProductModel> productsModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.productsModelArrayList = productsModelArrayList;


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productsModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


		if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.wish_list_item, parent, false);
		}

		TextView title1 = (TextView) view.findViewById(R.id.title);
		TextView priceNow =(TextView) view.findViewById(R.id.price);
		TextView quantity =(TextView) view.findViewById(R.id.quantity);
		ImageButton remove =(ImageButton) view.findViewById(R.id.remove);
		ImageButton cart =(ImageButton) view.findViewById(R.id.cart);
		ImageView image1 = (ImageView) view.findViewById(R.id.image);



		RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.rel);



		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
		view.setLayoutParams(new GridView.LayoutParams(params));


		title1.setText(productsModelArrayList.get(position).getProName());
		priceNow.setText("TK. "+productsModelArrayList.get(position).getProPriceNow());



		setCustomObjectListener(callback);

		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"view");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});


		Picasso.with(context).
				load(productsModelArrayList.get(position).getProImage())
				.placeholder(R.drawable.change_default_category)
				.error(R.drawable.change_default_category)
				.into(image1);


		quantity.setText(productsModelArrayList.get(position).getProQuantity());

		quantity.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"quantity");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});

		remove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"remove");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});
		cart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"cart");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});




		// Picasso.with(context).load(image.get(position)).into(imageView1);

		return view;
	}

	public interface onItemClickListener {
		public void onItemClicked(int position, String token);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		this.callback = listener;
	}



}
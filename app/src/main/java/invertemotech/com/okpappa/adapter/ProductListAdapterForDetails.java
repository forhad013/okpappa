package invertemotech.com.okpappa.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.activity.CategoryActivity;
import invertemotech.com.okpappa.activity.ProductDetailsActivity;
import invertemotech.com.okpappa.activity.ProductListActivity;
import invertemotech.com.okpappa.model.ProductsModel;
import invertemotech.com.okpappa.retrofitmodel.Product;
import invertemotech.com.okpappa.retrofitmodel.SimilarProducts;
import invertemotech.com.okpappa.util.DB;
import invertemotech.com.okpappa.util.SharePref;


public class ProductListAdapterForDetails extends RecyclerView.Adapter<ProductListAdapterForDetails.MyViewHolder> {
 onItemClickListener callback;

	DB db;
	ArrayList<SimilarProducts> ProductsModelArrayList ;
	Context context;
  SharePref sharePref;
	public ProductListAdapterForDetails(ArrayList<SimilarProducts> ProductsModelArrayList, Context context) {
		this.ProductsModelArrayList = ProductsModelArrayList;
		this.context = context;

		sharePref = new SharePref(context);

		db = new DB(context);
	}


	public class MyViewHolder extends RecyclerView.ViewHolder {

		ImageView imageView;
		RelativeLayout main;
		ProgressBar progressBar;
		TextView title;
		ImageButton cart,wish;
		public MyViewHolder(View view) {
			super(view);
			imageView=(ImageView) view.findViewById(R.id.image);

			title=(TextView) view.findViewById(R.id.title);
			cart = (ImageButton) view.findViewById(R.id.cart);
			wish = (ImageButton) view.findViewById(R.id.wish);



		}
	}



	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_item, parent, false);


		return new MyViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(final MyViewHolder holder, final int position) {


		int r =R.drawable.category;
//		holder.imageView.setImageResource(r);

		holder.title.setText(ProductsModelArrayList.get(position).getTitle());


		Picasso.with(context).
				load(ProductsModelArrayList.get(position).getMainimage())
				.placeholder(R.drawable.change_default_category)
				.error(R.drawable.change_default_category)
				.into(holder.imageView);


		setCustomObjectListener(callback);

		if(db.checkWishlistItem(ProductsModelArrayList.get(position).getId()+"")){
			holder.wish.setImageResource(R.drawable.ic_fav_active);
		}else{
			holder.wish.setImageResource(R.drawable.ic_fav);
		}

		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (callback != null) {
					callback.onItemClicked(position, "view");
				} else {

					Log.d("TEST", callback + "");
				}
			}
		});


		holder.wish.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (callback != null) {
					callback.onItemClicked(position, "wish");

					if(!db.checkWishlistItem(ProductsModelArrayList.get(position).getId()+"")){
						holder.wish.setImageResource(R.drawable.ic_fav_active);
					}else{
						holder.wish.setImageResource(R.drawable.ic_fav);
					}

				} else {

					Log.d("TEST", callback + "");
				}
			}
		});

		holder.cart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (callback != null) {
					callback.onItemClicked(position, "cart");
				} else {

					Log.d("TEST", callback + "");
				}
			}
		});

	}


	@Override
	public int getItemCount()
	{
		return ProductsModelArrayList.size();
	}



	public interface onItemClickListener {
		public void onItemClicked(int position, String token);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		this.callback = listener;
	}


}
package invertemotech.com.okpappa.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.model.CategoryModel;


@SuppressWarnings("unchecked")
public class ExpendableAdapter extends BaseExpandableListAdapter {

	public ArrayList<CategoryModel> categoryModelArrayList;

	public Activity activity;
	private final Context context;

	public LayoutInflater minflater;
	public ExpendableAdapter(Context context, ArrayList<CategoryModel> categoryModelArrayList) {
		this.context = context;

		this.categoryModelArrayList = categoryModelArrayList;

	}

	public void setInflater(LayoutInflater mInflater, Activity act) {
		this.minflater = mInflater;
		activity = act;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return null;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
							 boolean isLastChild, View convertView, ViewGroup parent) {


		if (convertView == null) {
			convertView =  LayoutInflater.from(context).inflate(
					R.layout.drawer_item_sub, parent, false);
			TextView text ;

			text = (TextView) convertView.findViewById(R.id.title);

			text.setText(categoryModelArrayList.get(groupPosition).getSubCategories().get(childPosition).getSubCategoryName());



			convertView.setTag(categoryModelArrayList.get(groupPosition).getSubCategories().get(childPosition));
		}

		return convertView;
	}




    @Override
	public int getChildrenCount(int groupPosition) {
		return categoryModelArrayList.get(groupPosition).getSubCategories().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return null;
	}

	@Override
	public int getGroupCount() {
		return categoryModelArrayList.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
							 View convertView, ViewGroup parent) {


		//if (convertView == null) {
			if(groupPosition==0) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item_signin, parent, false);



			}

			else {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item, parent, false);
				ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
				TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
				View bar = (View)convertView.findViewById(R.id.bar);
				txtTitle.setText(categoryModelArrayList.get(groupPosition).getCatName());
					convertView.setTag(categoryModelArrayList.get(groupPosition));

				if(categoryModelArrayList.get(groupPosition).getCatID().equals("007")){
					imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_camera));
				}
				if(categoryModelArrayList.get(groupPosition).getCatID().equals("008")){
					imgIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_com));
				}
				if(categoryModelArrayList.get(groupPosition).getCatID().equals("0001")){

					bar.setVisibility(View.VISIBLE);
				}else{
					bar.setVisibility(View.GONE);
				}

				if(groupPosition==1){

				}


			}
	//	}


		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}

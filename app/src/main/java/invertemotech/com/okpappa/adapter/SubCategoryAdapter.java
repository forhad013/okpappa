package invertemotech.com.okpappa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.model.SubCategoryModel;
import invertemotech.com.okpappa.retrofitmodel.Subcategory;


public class SubCategoryAdapter extends BaseAdapter {





	ArrayList<Subcategory> subCategoryModelArrayList;

	private Context context;

	public SubCategoryAdapter(Context context,
							  ArrayList<Subcategory> subCategoryModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.subCategoryModelArrayList = subCategoryModelArrayList;






	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return subCategoryModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.subcategory_item, parent, false);
	//	}

		TextView name = (TextView) view.findViewById(R.id.location);



//		Typeface tf = Typeface.createFromAsset(context.getAssets(),
//				"font/Siyamrupali.ttf");

		//String str="\u00bf";
		//name.setTypeface(tf);
		 String str=subCategoryModelArrayList.get(position).getTitle();
		//name.setText(locations.get(position).toString());
		name.setText(str);

		return view;
	}

}
package invertemotech.com.okpappa.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.model.SpinnerItem;
import invertemotech.com.okpappa.util.SharePref;


public class SpinnerListAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<String> navDrawerItems;

	private ArrayList<SpinnerItem> spinnerItemArrayList;

	public onProfilePicListener callback;

	String userID;
	SharePref sharePref;
	Typeface typeface;

	ArrayList<Boolean> isSelected;

	public SpinnerListAdapter(Context context, ArrayList<SpinnerItem> spinnerItemArrayList){
		this.context = context;

		this.spinnerItemArrayList = spinnerItemArrayList;
		sharePref = new SharePref(context);
		isSelected = new ArrayList<>();

		//initSelected();



		userID= sharePref.getshareprefdatastring(SharePref.USERID);
//		typeface = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd-Md.otf");

	}


//	public void initSelected(){
//
//
//		for(int i=0;i<spinnerItems.size();i++){
//			isSelected.add(i,false);
//		}
//	}
//
//
//	public void setSelected(int postition){
//		isSelected.clear();
//		for(int i=0;i<spinnerItems.size();i++){
//			if(i==postition){
//				isSelected.add(i, true);
//			}else {
//				isSelected.add(i, false);
//			}
//		}
//
//	}

	public interface onProfilePicListener {
		public void onProfilePicClicked();

	}

	public void setCustomObjectListener(onProfilePicListener callback) {
		Log.e("onCus","onCus");
		Log.e("listener",callback+" nm");
		this.callback = callback;


		Log.e("listenercallback",callback+" nm");
	}


	@Override
	public int getCount() {
		return spinnerItemArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return spinnerItemArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}



	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {


		if (convertView == null) {

			convertView = LayoutInflater.from(context).inflate(
					R.layout.spinner_item, parent, false);
		}


 

			TextView title = (TextView) convertView.findViewById(R.id.title);
			ImageView icon = (ImageView) convertView.findViewById(R.id.icon);

			title.setText(spinnerItemArrayList.get(position).getTitle());

			Log.e("is",spinnerItemArrayList.get(position).isSelected+"");


			if(spinnerItemArrayList.get(position).isSelected){

				title.setTextColor(Color.RED);
				title.setTextSize(18);

			}else{
				title.setTextSize(14);
				title.setTextColor(Color.GRAY);
			}







//


    
        
        return convertView;
	}
	
	

}

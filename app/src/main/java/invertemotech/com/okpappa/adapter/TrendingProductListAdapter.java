package invertemotech.com.okpappa.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.retrofitmodel.ExclusiveProduct;
import invertemotech.com.okpappa.retrofitmodel.TodaysTrending;
import invertemotech.com.okpappa.util.DB;


public class TrendingProductListAdapter extends BaseAdapter {


	DB db;

	ArrayList<TodaysTrending> productsModelArrayList;
	int tag;
    String token;

	onItemClickListener callback;
	private Context context;

	public TrendingProductListAdapter(Context context,
                                      ArrayList<TodaysTrending> productsModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.productsModelArrayList = productsModelArrayList;
		db = new DB(context);


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productsModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


		//if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.product_list_item, parent, false);
		//}

			TextView title1 = (TextView) view.findViewById(R.id.title);
			TextView priceNow =(TextView) view.findViewById(R.id.priceNow);
			TextView priceprevious =(TextView) view.findViewById(R.id.pricePrevious);
			ImageView image1 = (ImageView) view.findViewById(R.id.image);
		ImageButton cart = (ImageButton) view.findViewById(R.id.cart);
		final ImageButton wish = (ImageButton) view.findViewById(R.id.wish);

			RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.rel);



		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
		view.setLayoutParams(new GridView.LayoutParams(params));



		title1.setText(productsModelArrayList.get(position).getTitle());
		priceNow.setText("TK. "+productsModelArrayList.get(position).getCurrentprice());
		priceprevious.setText("TK. "+productsModelArrayList.get(position).getOldprice());

		Picasso.with(context).
				load(productsModelArrayList.get(position).getMainimage())
				.placeholder(R.drawable.grocery)
				.error(R.drawable.grocery)
				.into(image1);


		setCustomObjectListener(callback);


		if(db.checkWishlistItem(productsModelArrayList.get(position).getId()+"")){
			wish.setImageResource(R.drawable.ic_fav_active);
		}else{
			wish.setImageResource(R.drawable.ic_fav);
		}

		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"view");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});



		wish.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){

					boolean isWished = db.checkWishlistItem(productsModelArrayList.get(position).getId()+"");

					if(isWished){
						wish.setImageResource(R.drawable.ic_fav);
					}else{
						wish.setImageResource(R.drawable.ic_fav_active);
					}
					callback.onItemClicked(position,"wish");



				}else{

					Log.d("TEST", callback+"");
				}
			}
		});

		cart.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(callback!=null){
					callback.onItemClicked(position,"cart");
				}else{

					Log.d("TEST", callback+"");
				}
			}
		});

		// Picasso.with(context).load(image.get(position)).into(imageView1);




		return view;
	}

	public interface onItemClickListener {
		public void onItemClicked(int position, String token);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		this.callback = listener;
	}



}
package invertemotech.com.okpappa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.List;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.model.AutoCompleteModel;

/**
 * Created by Admin on 12/15/2015.
 */
public class SearchAutoCompleteAdapter extends ArrayAdapter<AutoCompleteModel> {

    private Context context;
    int resource, textViewResourceId, layoutResourceId;
    List<AutoCompleteModel> items, tempItems, suggestions;
    AutoCompleteModel data[] = null;

    public SearchAutoCompleteAdapter(Context context, int layoutResourceId, AutoCompleteModel[] data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    /*public SearchAutoCompleteAdapter(Context context, int resource, int textViewResourceId, List<AutoCompleteModel> items) {
        super(context, resource, textViewResourceId, items);
        this.context = context;
        this.resource = resource;
        this.textViewResourceId = textViewResourceId;
        this.items = items;
        tempItems = new ArrayList<AutoCompleteModel>(items); // this makes the difference.
        suggestions = new ArrayList<AutoCompleteModel>();
    }*/

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //convertView = null;
        View view = convertView;

        try {

            /*
             * The convertView argument is essentially a "ScrapView" as described is Lucas post
             * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
             * It will have a non-null value when ListView is asking you recycle the row layout.
             * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
             */
            if (view == null) {
                // inflate the layout
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);//((SearchActivity) context).getLayoutInflater(); //
                view = inflater.inflate(layoutResourceId, parent, false);
            }

            // object item based on the position
            AutoCompleteModel objectItem = data[position];
            if (objectItem != null) {
                TextView autoTitle = (TextView) view.findViewById(R.id.title);
                if (autoTitle != null) {
                    autoTitle.setText(objectItem.getAutoTitle());
                }


            }
            // in case you want to add some style, you can do something like:
            //textViewItem.setBackgroundColor(Color.CYAN);

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_row_autocomplete, parent, false);
        }
        AutoCompleteModel autoItems = items.get(position);
        if (autoItems != null) {
            TextView autoTitle = (TextView) view.findViewById(R.id.autoComplete_item_title_tv);
            if (autoTitle != null) {
                autoTitle.setText(autoItems.getAutoTitle());
            }

            TextView autoType = (TextView) view.findViewById(R.id.autoComplete_item_type_tv);
            if (autoType != null) {
                autoType.setText(autoItems.getAutoType());
            }

            TextView autoCount = (TextView) view.findViewById(R.id.autoComplete_item_count_tv);
            if (autoCount != null) {
                autoCount.setText(autoItems.getAutoCount());
            }

        }*/
        return view;
    }

   /* @Override
    public Filter getFilter() {
        return nameFilter;
    }


    *//**Custom Filter implementation for custom suggestions we provide.*//*

    Filter nameFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            String str = ((AutoCompleteModel) resultValue).getAutoTitle();
            return str;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (AutoCompleteModel autoItem : tempItems) {
                    if (autoItem.getAutoTitle().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        suggestions.add(autoItem);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<AutoCompleteModel> filterList = (ArrayList<AutoCompleteModel>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (AutoCompleteModel autoItem : filterList) {
                    add(autoItem);
                    notifyDataSetChanged();
                }
            }
        }
    };*/
}
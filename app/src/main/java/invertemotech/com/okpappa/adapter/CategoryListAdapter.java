package invertemotech.com.okpappa.adapter;


import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.activity.CategoryActivity;
import invertemotech.com.okpappa.model.CategoryModel;
import invertemotech.com.okpappa.retrofitmodel.Category;
import invertemotech.com.okpappa.retrofitmodel.Subcategory;
import invertemotech.com.okpappa.util.SharePref;


public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.MyViewHolder> {


	ArrayList<Category> categoryModelArrayList ;
	Context context;
  SharePref sharePref;
	public CategoryListAdapter(ArrayList<Category> categoryModelArrayList, Context context) {
		this.categoryModelArrayList = categoryModelArrayList;
		this.context = context;

		sharePref = new SharePref(context);
	}


	public class MyViewHolder extends RecyclerView.ViewHolder {

		ImageView imageView;
		RelativeLayout main;
		ProgressBar progressBar;
		TextView title;
		public MyViewHolder(View view) {
			super(view);
			imageView=(ImageView) view.findViewById(R.id.image);

			title=(TextView) view.findViewById(R.id.title);




		}
	}



	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_item, parent, false);


		return new MyViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(final MyViewHolder holder, final int position) {


		int r =R.drawable.category;
		holder.imageView.setImageResource(r);

		holder.title.setText(categoryModelArrayList.get(position).getTitle());


		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String categoryName = categoryModelArrayList.get(position).getTitle();
				String categoryId = categoryModelArrayList.get(position).getId()+"";

				ArrayList<Subcategory> subcategoryArrayList = new ArrayList<Subcategory>();
				subcategoryArrayList = categoryModelArrayList.get(position).getSubcategories();


				Intent i = new Intent(context,CategoryActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);


				Parcelable listParcelable = Parcels.wrap(subcategoryArrayList);
				i.putExtra("subcategory", listParcelable);
				i.putExtra("categoryName", categoryName);
				i.putExtra("categoryId", categoryId);

				context.getApplicationContext().startActivity(i);

			}
		});


		Picasso.with(context).
				load(categoryModelArrayList.get(position).getImage())
				.placeholder(R.drawable.change_default_category)
				.error(R.drawable.change_default_category)
				.into(holder.imageView);

	}


	@Override
	public int getItemCount()
	{
		return categoryModelArrayList.size();
	}
}
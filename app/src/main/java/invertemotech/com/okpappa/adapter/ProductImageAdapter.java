package invertemotech.com.okpappa.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.model.ImageModel;
import invertemotech.com.okpappa.util.SharePref;


public class ProductImageAdapter extends RecyclerView.Adapter<ProductImageAdapter.MyViewHolder> {


	onItemClickListener callback;

	ArrayList<ImageModel> imageModelArrayList ;
	Context context;
  SharePref sharePref;
	public ProductImageAdapter(ArrayList<ImageModel> imageModelArrayList, Context context) {
		this.imageModelArrayList = imageModelArrayList;
		this.context = context;

		sharePref = new SharePref(context);
	}


	public class MyViewHolder extends RecyclerView.ViewHolder {

		ImageView imageView;
		RelativeLayout main;
		ProgressBar progressBar;
		TextView title;
		public MyViewHolder(View view) {
			super(view);
			imageView=(ImageView) view.findViewById(R.id.image);






		}
	}



	@Override
	public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_list_item, parent, false);


		return new MyViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(final MyViewHolder holder, final int position) {


		int r =R.drawable.category;
		holder.imageView.setImageResource(r);

	//	holder.title.setText(categoryModelArrayList.get(position).getCatName());

		Picasso.with(context).load(imageModelArrayList.get(position).getUrl()).into(holder.imageView);

		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(callback!=null){
					callback.onItemClicked(position);
				}else{

					Log.d("TEST", callback+"");
				}


			}
		});


		setCustomObjectListener(callback);





	}


	public interface onItemClickListener {
		public void onItemClicked(int position);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		this.callback = listener;
	}



	@Override
	public int getItemCount()
	{
		return imageModelArrayList.size();
	}
}
package invertemotech.com.okpappa.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.adapter.ExpendableAdapter;
import invertemotech.com.okpappa.adapter.SearchAutoCompleteAdapter;
import invertemotech.com.okpappa.fragment.Fragment_home;
import invertemotech.com.okpappa.model.AutoCompleteModel;
import invertemotech.com.okpappa.model.CategoryModel;
import invertemotech.com.okpappa.model.SubCategoryModel;
import invertemotech.com.okpappa.retrofit.ApiClient;
import invertemotech.com.okpappa.retrofit.ApiInterafce;
import invertemotech.com.okpappa.retrofitmodel.AutoCompleteData;
import invertemotech.com.okpappa.retrofitmodel.AutoCompleteResponse;
import invertemotech.com.okpappa.retrofitmodel.SplashData;
import invertemotech.com.okpappa.util.ConnectionDetector;
import invertemotech.com.okpappa.util.DB;
import invertemotech.com.okpappa.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements Animation.AnimationListener {
    private AutoCompleteModel[] objectItemData;
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerList;
    private ImageView mDrawerfooter;
    private ImageButton mDrawerToggle, myProfile, addPoll,notification;

    public int newMessageCounter = 0;

    TextView changePicture;

    ArrayList<AutoCompleteData> autoCompleteProducts;

    DB db;
    String search_item;


    ArrayList<CategoryModel> categoryModelArrayList;

  SharePref sharePref;
    boolean isInternetOn;
   ExpendableAdapter adapter;
    private ArrayList<String> navDrawerItems;
    private Uri mCropImageUri;
    int userid;
  ConnectionDetector cd;
    private ArrayList<Integer> navDrawerItemsImages;
    // Animation
    Animation animRotate;
    AutoCompleteTextView searchBox;
    ImageButton cartBtn;
   public SplashData splashData;
    TextView cartCounter;
    private SearchAutoCompleteAdapter autoCompleteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        sharePref = new SharePref(getApplicationContext());
        db = new DB(getApplicationContext());


        splashData = new SplashData();

        splashData = Parcels.unwrap(getIntent().getParcelableExtra("splashdata"));


        String lg = sharePref.getshareprefdatastring(SharePref.LANGUAGE);
        Log.e("set",splashData.getBanners().size()+"");



        autoCompleteProducts = new ArrayList<>();


        searchBox = (AutoCompleteTextView) findViewById(R.id.searchBox);

        cartBtn = (ImageButton) findViewById(R.id.cart);

//        animRotate = AnimationUtils.loadAnimation(getApplicationContext(),
//                R.anim.rotate);

        // set animation listener
    //    animRotate.setAnimationListener(this);

        cd = new ConnectionDetector(getApplicationContext());


        isInternetOn = cd.isConnectingToInternet();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);

        mDrawerToggle = (ImageButton) findViewById(R.id.menu);


        cartCounter = (TextView) findViewById(R.id.cartCounter);


        sharePref = new SharePref(getApplicationContext());


        AutoCompleteModel[] data = new AutoCompleteModel[0];
        autoCompleteAdapter = new SearchAutoCompleteAdapter(this, R.layout.auto_complete_list_item, data);
        searchBox.setAdapter(autoCompleteAdapter);


        searchBox.setHint("Search products");
        searchBox.setThreshold(2);




        searchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    Intent intent = new Intent(MainActivity.this,ProductListActivity.class);
                    intent.putExtra("token",  searchBox.getText().toString());
                    intent.putExtra("subcategoryId", "0");
                    intent.putExtra("search", true);

                    startActivity(intent);


                    return true;
                }

                return false;
            }
        });








//        mDrawerList.setAdapter(adapter);




        setCartCounter();



        mDrawerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }

             //   mDrawerToggle.startAnimation(animRotate);
            }
        });






        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,CartActivity.class);
                     startActivity(intent);
            }
        });




        LoadHome();
       mDrawerList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
           @Override
           public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
               if(categoryModelArrayList.get(i).getCatID().equals("0010")){
                   Intent a= new Intent(MainActivity.this,WishListActivity.class);
                   startActivity(a);
                   displayView(i);
               }else if(categoryModelArrayList.get(i).getCatID().equals("002")){
                   displayView(i);
               }else if(categoryModelArrayList.get(i).getCatID().equals("001")){
                   Intent a= new Intent(MainActivity.this,LoginActivity.class);
                   startActivity(a);
                   mDrawerLayout.closeDrawer(mDrawerList);
               }


               return false;
           }
       });


        mDrawerList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                String subcategoryId = categoryModelArrayList.get(groupPosition).getSubCategories().get(childPosition).subCategoryID;

                Intent intent = new Intent(MainActivity.this,ProductListActivity.class);
                intent.putExtra("subcategoryId", subcategoryId);
                intent.putExtra("token",  "");
                intent.putExtra("search", false);
                startActivity(intent);
                return false;
            }
        });



        searchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.e("id",autoCompleteProducts.get(position).getId()+"");

                searchBox.setText("");
                Intent it = new Intent(getApplicationContext(),ProductDetailsActivity.class);
                it.putExtra("productId",autoCompleteProducts.get(position).getId()+"");
                startActivity(it);

            }
        });



        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = String.valueOf(s);
                search_item = temp;
                //Toast.makeText(context,search_item,Toast.LENGTH_SHORT).show();
                if (search_item.trim().length() > 0) {

                    search_item = searchBox.getText().toString();


                    searchItems();
                }
            }
        });

    }


    public void setCartCounter(){



        cartCounter.setText(db.getCartCount());
    }




    public void searchItems(){




        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);





        Call<AutoCompleteResponse> call = apiService.getAutoComplete(search_item);



        call.enqueue(new Callback<AutoCompleteResponse>() {
            @Override
            public void onResponse(Call<AutoCompleteResponse> call, Response<AutoCompleteResponse> response) {


                try {


                    boolean error = response.body().getError();


                    if (!error) {


                        autoCompleteProducts = response.body().getData();

                        setAutoCompleteData();
                    } else {

                    }


                }catch (Exception e){

                    e.printStackTrace();


               //     Toast.makeText(getApplicationContext(),"No More ProductDetails",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AutoCompleteResponse> call, Throwable t) {

            }
        });


    }


    public void setAutoCompleteData(){

        objectItemData = new AutoCompleteModel[autoCompleteProducts.size()];

        for(int i=0;i<autoCompleteProducts.size();i++) {
            AutoCompleteModel autoItem = new AutoCompleteModel(autoCompleteProducts.get(i).getId()+""
                    , autoCompleteProducts.get(i).getTitle());
            objectItemData[i] = autoItem;
        }

        autoCompleteAdapter.notifyDataSetChanged();
        autoCompleteAdapter = new SearchAutoCompleteAdapter(getApplicationContext(), R.layout.auto_complete_list_item, objectItemData);
        searchBox.setAdapter(autoCompleteAdapter);

    }


    public void setSearch(){






    }
    @Override
    protected void onResume() {


        categoryModelArrayList = new ArrayList<>();


        setCategory();
        adapter = new ExpendableAdapter(getApplicationContext(), categoryModelArrayList);


        mDrawerList.setAdapter(adapter);

        super.onResume();



    }


    public void setCategory(){

        ArrayList<SubCategoryModel> subCategoryModelArrayList111;

        subCategoryModelArrayList111 = new ArrayList<>();
        CategoryModel categoryModel111 = new CategoryModel("001", "SIGN IN", subCategoryModelArrayList111);
        categoryModelArrayList.add(categoryModel111);

        ArrayList<SubCategoryModel> subCategoryModelArrayList11;
        subCategoryModelArrayList11 = new ArrayList<>();
        CategoryModel categoryModel11 = new CategoryModel("002", "Home", subCategoryModelArrayList11);
        categoryModelArrayList.add(categoryModel11);

        for(int k=0;k<splashData.getCategories().size();k++) {
            ArrayList<SubCategoryModel> subCategoryModelArrayList;

            subCategoryModelArrayList = new ArrayList<>();

            for (int i = 0; i < splashData.getCategories().get(k).getSubcategories().size(); i++) {

                SubCategoryModel subCategoryModel = new SubCategoryModel(
                        splashData.getCategories().get(k).getSubcategories().get(i).getId()+"",
                        splashData.getCategories().get(k).getSubcategories().get(i).getTitle());

                subCategoryModelArrayList.add(subCategoryModel);
            }
            CategoryModel categoryModel = new CategoryModel(
                    splashData.getCategories().get(k).getId()+"",
                    splashData.getCategories().get(k).getTitle(),
                    subCategoryModelArrayList);


            categoryModelArrayList.add(categoryModel);
        }


        ArrayList<SubCategoryModel> subCategoryModelArrayList = new ArrayList<>();


        CategoryModel categoryModel = new CategoryModel("007", "Order By Picture", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel);

        CategoryModel categoryModel1 = new CategoryModel("008", "Commuinty", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel1);

        CategoryModel categoryModel2 = new CategoryModel("009", "My Account", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel2);

        CategoryModel categoryModel3 = new CategoryModel("0010", "Wishlist", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel3);

        CategoryModel categoryModel4 = new CategoryModel("0011", "Track Order", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel4);

        CategoryModel categoryModel5 = new CategoryModel("0012", "My Cart", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel5);


        CategoryModel categoryModel6 = new CategoryModel("0013", "Checkout", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel6);

//        CategoryModel categoryModel7 = new CategoryModel("101", "Login", subCategoryModelArrayList);
//        categoryModelArrayList.add(categoryModel7);

        CategoryModel categoryModel8 = new CategoryModel("0014", "Settings", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel8);

        CategoryModel categoryModel9 = new CategoryModel("0015", "Call us at: +8801674454284", subCategoryModelArrayList);
        categoryModelArrayList.add(categoryModel9);






    }

    @Override
    protected void onDestroy() {
        if(isInternetOn){

        }
        super.onDestroy();
    }





    public void LoadHome() {

        Fragment fragment = new Fragment_home();
        FragmentManager mFragmentManager = getSupportFragmentManager();

        FragmentTransaction ft = mFragmentManager.beginTransaction();


        ft.replace(R.id.frame_container, fragment);

        ft.commit();


    }



    public void displayView(int position) {



        switch (position) {
            case 0:

                break;
            case 1:

//                String link= "https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName();
//
//                        Intent intent6 = new Intent();
//                intent6.setAction(Intent.ACTION_SEND);
//
//                intent6.setType("text/plain");
//                intent6.putExtra(Intent.EXTRA_TEXT, link);
//                startActivity(Intent.createChooser(intent6, "Share"));

                LoadHome();

                break;


            case 2:
            //  LoadHome();

                break;
            case 3:


                break;
            case 4:


                break;
            case 5:


                break;
            case 6:

                break;
            case 7:

                break;
            case 8:



                break;

            case 9:


                break;


            default:
                break;
        }


        mDrawerList.setSelection(position);

        mDrawerLayout.closeDrawer(mDrawerList);
    }




    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

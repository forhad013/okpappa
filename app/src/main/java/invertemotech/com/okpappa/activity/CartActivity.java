package invertemotech.com.okpappa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import android.widget.TextView;

import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.adapter.CartListAdapter;
import invertemotech.com.okpappa.model.CartProductModel;
import invertemotech.com.okpappa.util.DB;
import invertemotech.com.okpappa.util.ExpandedListView;

public class CartActivity extends AppCompatActivity {
    PickerUI mPickerUI;
    TextView sortBy;
    List<String> options;

    DB db;

    float subTotalAmount,totalAmount,discountAmonut;

    NumberPicker horizontal_number_picker;

    int cuurentPosition;
    Button addQuantity;
    Button checkoutBtn;
    LinearLayout pickerLin;
   // NumberPicker numberPicker;
    LinearLayout lin;

    final String[] arr1=new String[]{"1","2","3","4","5"};

    ArrayList<Integer> quantityList = new ArrayList<>();

    ExpandedListView list;
    CartListAdapter cartListAdapter;
    ArrayList<CartProductModel> productsModelArrayList;
    TextView totalTxt,subTotalTxt,discountTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        list = (ExpandedListView) findViewById(R.id.list);

        db = new DB(getApplicationContext());

        for(int i=0;i<50;i++){
            quantityList.add(i+1);
        }

        pickerLin = (LinearLayout) findViewById(R.id.pickerLin);
        horizontal_number_picker = (NumberPicker) findViewById(R.id.horizontal_number_picker);
        addQuantity = (Button) findViewById(R.id.addQuantity);
        lin = (LinearLayout) findViewById(R.id.lin);
        totalTxt = (TextView) findViewById(R.id.total);
        subTotalTxt = (TextView) findViewById(R.id.subtotal);
        discountTxt = (TextView) findViewById(R.id.discount);
        options = Arrays.asList(arr1);

        checkoutBtn = (Button) findViewById(R.id.checkOut);




        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(CartActivity.this,CheckOutActivity.class);
                startActivity(it);
            }
        });







        setList();


        setCartCounter();

    }

    public void setCartCounter(){


    //    TextView cartCounter = (TextView) findViewById(R.id.cartCounter);


      //  cartCounter.setText(db.getCartCount());
    }



    public static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }

            Log.e("h1",(totalHeight/2)+"");

            ViewGroup.LayoutParams params = listView.getLayoutParams();
           // params.height = (totalHeight/2)-(listAdapter.getCount()*25);
            params.height = (totalHeight/2 - (listView.getDividerHeight() * (listAdapter.getCount() - 1))-listAdapter.getCount()*25);
            listView.setLayoutParams(params);
            listView.requestLayout();
            Log.e("h1",params.height+"");

        }
    }

    public void setList(){

        productsModelArrayList = new ArrayList<>();
        productsModelArrayList = db.getCartProducts();


        cartListAdapter = new CartListAdapter(CartActivity.this,productsModelArrayList);
        list.setAdapter(cartListAdapter);
        Utility.setListViewHeightBasedOnChildren(list);

        cartListAdapter.setCustomObjectListener(new CartListAdapter.onItemClickListener() {
            @Override
            public void onItemClicked(int position, String token) {

                setCalculation();

                if(token.equals("quantity")){
                    pickerLin.setVisibility(View.VISIBLE);

                     String q=  productsModelArrayList.get(position).getProQuantity();

                    cuurentPosition = position;

                }else if(token.equals("remove")){

                    db.removeFromCart(productsModelArrayList.get(position).getProID());
//                     productsModelArrayList.clear();
//                    productsModelArrayList = db.getCartProducts();

                     setList();

                }else if(token.equals("view")){



                    Intent it = new Intent(CartActivity.this,ProductDetailsActivity.class);
                    it.putExtra("productId",productsModelArrayList.get(cuurentPosition).getProID()+"");
                    startActivity(it);

                }
            }
        });

        addQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              int  productQuantity= horizontal_number_picker.getValue();


                productsModelArrayList.get(cuurentPosition).setProQuantity(productQuantity+"");
                cartListAdapter.notifyDataSetChanged();
                pickerLin.setVisibility(View.GONE);

                db.updateQuantity(productsModelArrayList.get(cuurentPosition).getProID(),productQuantity+"");
            }
        });

        setCalculation();

    }

    public void setCalculation(){




        subTotalAmount = 0;

        for(int i=0;i<productsModelArrayList.size();i++){

            subTotalAmount+=Float.parseFloat(productsModelArrayList.get(i).getProQuantity())*Float.parseFloat(productsModelArrayList.get(i).getProPriceNow());
        }

        subTotalTxt.setText(subTotalAmount+"");
        totalTxt.setText(subTotalAmount+"");
    }
}

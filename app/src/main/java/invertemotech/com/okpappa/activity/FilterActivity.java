package invertemotech.com.okpappa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;

import org.parceler.Parcels;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.adapter.FilterAdapter;
import invertemotech.com.okpappa.model.CategoryModel;
import invertemotech.com.okpappa.model.SubCategoryModel;
import invertemotech.com.okpappa.retrofit.ApiClient;
import invertemotech.com.okpappa.retrofit.ApiInterafce;
import invertemotech.com.okpappa.retrofitmodel.Brand;
import invertemotech.com.okpappa.retrofitmodel.Category;
import invertemotech.com.okpappa.retrofitmodel.CategoryBrandResponse;
import invertemotech.com.okpappa.retrofitmodel.SplashData;
import invertemotech.com.okpappa.retrofitmodel.CategoryBrandResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static invertemotech.com.okpappa.R.id.menu;


public class FilterActivity extends AppCompatActivity {
    ArrayList<String> category = new ArrayList<>();
    ArrayList<String> brandArray = new ArrayList<>();
      CrystalRangeSeekbar rangeSeekbar ;

    TextView minPrice,maxPrice,brand;
    TextView catTitle,subCatTitle;
    ListView categoryList;
    ListView brandList;

    FilterAdapter categoryAdapter;
    FilterAdapter brandAdapter;
    int selectedCategoryPosition=0;
    ArrayList<Category> categoryArrayList;
    ArrayList<Brand> brandArrayList;
    Button doneBtn;

    int brandId,subCategoryId;
    String price1,price2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        rangeSeekbar = (CrystalRangeSeekbar)findViewById(R.id.priceRange);

        minPrice = (TextView) findViewById(R.id.minPrice);
        maxPrice = (TextView) findViewById(R.id.maxPrice);
        catTitle = (TextView) findViewById(R.id.catTitle);
        subCatTitle = (TextView) findViewById(R.id.subCatTitle);
        brand = (TextView) findViewById(R.id.brand);
        categoryList = (ListView) findViewById(R.id.categoryList);
        brandList = (ListView) findViewById(R.id.brandList);
        doneBtn = (Button) findViewById(R.id.done);


        categoryArrayList = new ArrayList<>();
        brandArrayList = new ArrayList<>();




        categoryList.setSelection(0);

        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(catTitle.getVisibility()==View.GONE){
                    catTitle.setVisibility(View.VISIBLE);
                    catTitle.setText(categoryArrayList.get(i).getTitle());
                    selectedCategoryPosition = i;

                    catTitle.setBackgroundColor(getResources().getColor(R.color.btn_color));
                    catTitle.setTextColor(getResources().getColor(R.color.whitetext));

                    categoryList.setVisibility(View.VISIBLE);
                    category = new ArrayList<>();

                    for(int k =0;k<categoryArrayList.get(i).getSubcategories().size();k++){

                        category.add(categoryArrayList.get(i).getSubcategories().get(k).getTitle());

                    }

                    subCategoryId = 0;
                    categoryAdapter = new FilterAdapter(getApplicationContext(),category);
                    categoryList.setAdapter(categoryAdapter);
                    Utility.setListViewHeightBasedOnChildren(categoryList);


                }else{
                    subCatTitle.setBackgroundColor(getResources().getColor(R.color.btn_color));
                    subCatTitle.setTextColor(getResources().getColor(R.color.whitetext));
                    catTitle.setBackgroundColor(getResources().getColor(R.color.whitetext));
                    catTitle.setTextColor(getResources().getColor(R.color.black));

                    subCategoryId = categoryArrayList.get(selectedCategoryPosition).getSubcategories().get(i).getId();
                    subCatTitle.setVisibility(View.VISIBLE);
                    subCatTitle.setText(categoryArrayList.get(selectedCategoryPosition).getSubcategories().get(i).getTitle());
                    categoryList.setVisibility(View.GONE);
                }
            }
        });


        catTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setData();
            }
        });

        subCatTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                subCatTitle.setVisibility(View.GONE);
                categoryList.setVisibility(View.VISIBLE);

                category = new ArrayList<>();

                for(int k =0;k<categoryArrayList.get(selectedCategoryPosition).getSubcategories().size();k++){

                    category.add(categoryArrayList.get(selectedCategoryPosition).getSubcategories().get(k).getTitle());

                }
                categoryAdapter = new FilterAdapter(getApplicationContext(),category);
                categoryList.setAdapter(categoryAdapter);
                Utility.setListViewHeightBasedOnChildren(categoryList);

            }
        });


        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("price11",price1+"");
                Log.e("price21",price2+"");

                Intent data = new Intent();
                data.putExtra("price1",price1+"");
                data.putExtra("price2",price2+"");
                data.putExtra("brandId",brandId+"");
                data.putExtra("subcategoryId",subCategoryId+"");

                setResult(RESULT_OK,data);
                finish();

            }
        });


        brandList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                brandList.setVisibility(View.GONE);
                brand.setVisibility(View.VISIBLE);
                brand.setText(brandArray.get(i).toString());

                brand.setBackgroundColor(getResources().getColor(R.color.btn_color));
                brand.setTextColor(getResources().getColor(R.color.whitetext));

                brandId = brandArrayList.get(i).getId();
            }
        });



        getData();

        brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBrand();
            }
        });

        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minPrice.setText("TK "+String.valueOf(minValue));
                maxPrice.setText("TK "+String.valueOf(maxValue));

                price1  = String.valueOf(minValue);
                price2  = String.valueOf(maxValue);
            }
        });

        ImageButton backBtn = (ImageButton) findViewById(R.id.bckBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
            }
        });

    }


    @Override
    public void onBackPressed() {

        Intent data = new Intent();
        data.putExtra("price1",price1+"");
        data.putExtra("price2",price2+"");
        data.putExtra("brandId",brandId+"");
        data.putExtra("subcategoryId",subCategoryId+"");

        setResult(RESULT_OK,data);
        finish();
        super.onBackPressed();
    }

    public static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));

            Log.e("h",params.height+"");
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }





    public void getData(){


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);




        Call<CategoryBrandResponse> call = apiService.getCategoryBrand();


        call.enqueue(new Callback<CategoryBrandResponse>() {
            @Override
            public void onResponse(Call<CategoryBrandResponse>call, Response<CategoryBrandResponse> response) {



                try {

                    categoryArrayList = response.body().getData().getCategories();

                    brandArrayList = response.body().getData().getBrands();

                    setData();
                    
                }catch (Exception e){

                    e.printStackTrace();


                }



            }

            @Override
            public void onFailure(Call<CategoryBrandResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());


            }
        });


    }

    public void setData(){


        catTitle.setVisibility(View.GONE);
        subCatTitle.setVisibility(View.GONE);

        catTitle.setVisibility(View.VISIBLE);
        subCatTitle.setVisibility(View.VISIBLE);

        catTitle.setVisibility(View.GONE);
        subCatTitle.setVisibility(View.GONE);

        categoryList.setVisibility(View.VISIBLE);



       category = new ArrayList<>();

        for(int i =0;i<categoryArrayList.size();i++){

            category.add(categoryArrayList.get(i).getTitle());

        }


        categoryAdapter = new FilterAdapter(getApplicationContext(),category);
        categoryList.setAdapter(categoryAdapter);

        DisplayMetrics metrics;
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        ViewGroup.LayoutParams params = categoryList.getLayoutParams();
        params.height = categoryAdapter.getCount()* getDPI(45,metrics);;

        categoryList.setLayoutParams(params);
        categoryList.requestLayout();


        Log.e("h",params.height+"");



        setBrand();


    }


    public void setBrand(){
        brandList.setVisibility(View.VISIBLE);
        brandArray = new ArrayList<>();

        for(int i =0;i<brandArrayList.size();i++){

            brandArray.add(brandArrayList.get(i).getTitle());

        }


        brandAdapter = new FilterAdapter(getApplicationContext(),brandArray);
        brandList.setAdapter(brandAdapter);

        DisplayMetrics metrics1;
        metrics1 = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics1);

        ViewGroup.LayoutParams params1 = brandList.getLayoutParams();
        params1.height = brandAdapter.getCount()* getDPI(45,metrics1);;

        brandList.setLayoutParams(params1);
        brandList.requestLayout();

        //  Utility.setListViewHeightBasedOnChildren(categoryList);
    }

    public static int getDPI(int size, DisplayMetrics metrics){
        return (size * metrics.densityDpi) / DisplayMetrics.DENSITY_DEFAULT;
    }
}

package invertemotech.com.okpappa.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.ArrayList;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.adapter.SubCategoryAdapter;
import invertemotech.com.okpappa.model.SubCategoryModel;
import invertemotech.com.okpappa.retrofitmodel.Subcategory;

import static invertemotech.com.okpappa.R.id.menu;

public class CategoryActivity extends AppCompatActivity {
    ArrayList<Boolean> checkList;
    ArrayList<String> brandName;
    ListView list;
    TextView locstionTitle;

    Context context;

    ArrayList<SubCategoryModel> subCategoryModelArrayList;
    ListView subCategoryList; 
    Button done;
    TextView title1,title2,title3,title4;

    ArrayList<Subcategory> subcategoryArrayList;
    String categoryId;
    String categoryName;

    ImageView backBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
       overridePendingTransition(R.anim.slidein_from_bottom, R.anim.nothing);

        subCategoryList = (ListView) findViewById(R.id.list);

        backBtn = (ImageView) findViewById(R.id.menu);

        subcategoryArrayList = Parcels.unwrap(getIntent().getParcelableExtra("subcategory"));
        categoryId = getIntent().getStringExtra("categoryId");
        categoryName = getIntent().getStringExtra("categoryName");

        context = CategoryActivity.this;
        title1 = (TextView) findViewById(R.id.title1);
        title2 = (TextView) findViewById(R.id.title2);
        title3 = (TextView) findViewById(R.id.title3);
        title4 = (TextView) findViewById(R.id.title4);



        title1.setText(categoryName);




        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SubCategoryAdapter subCategoryAdapter = new SubCategoryAdapter(context,subcategoryArrayList);

        subCategoryList.setAdapter(subCategoryAdapter);

        Utility.setListViewHeightBasedOnChildren(subCategoryList);



        subCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(context,ProductListActivity.class);
                intent.putExtra("subcategoryId", subcategoryArrayList.get(i).getId()+"");
                intent.putExtra("token",  "");
                intent.putExtra("search", false);
                startActivity(intent);
            }
        });


        ImageButton backBtn = (ImageButton) findViewById(menu);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        

    }

    public static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }

    }

        @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.anim.slide_up, R.anim.slideout_to_bottom);

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {



        super.onDestroy();
    }

 

 
 

 
}

package invertemotech.com.okpappa.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.adapter.EndlessScrollListener;
import invertemotech.com.okpappa.adapter.ProductListAdapter;
import invertemotech.com.okpappa.adapter.SearchAutoCompleteAdapter;
import invertemotech.com.okpappa.adapter.SpinnerListAdapter;
import invertemotech.com.okpappa.model.AutoCompleteModel;
import invertemotech.com.okpappa.model.ProductsModel;
import invertemotech.com.okpappa.model.SpinnerItem;
import invertemotech.com.okpappa.retrofit.ApiClient;
import invertemotech.com.okpappa.retrofit.ApiInterafce;
import invertemotech.com.okpappa.retrofitmodel.AutoCompleteData;
import invertemotech.com.okpappa.retrofitmodel.AutoCompleteResponse;
import invertemotech.com.okpappa.retrofitmodel.Product;
import invertemotech.com.okpappa.retrofitmodel.ProductFilterResponse;
import invertemotech.com.okpappa.util.DB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static invertemotech.com.okpappa.R.id.menu;

public class ProductListActivity extends AppCompatActivity {

    DB db;
    private AutoCompleteModel[] objectItemData;
    //sortkey=> 1->pricelowest, 2->pricehighest, 3->nameasc, 4->namedesc

    final String[] arr1=new String[]{"Lowest Price","Highest Price","Name A to Z","Name Z to A"};

    String brandId = "0",subCategoryId="0",price1="0",price2="0",subCateogryName="";

    int RESPONSE_CODE_FILTER = 123;

    GridView productList;
    ArrayList<AutoCompleteData> autoCompleteProducts;
    ProductListAdapter productListAdapter;

    TextView sortBy,filter,title;
    ArrayList<String> options;
    int sortkey=1, pageNumber=1;

    String term ="0";
    String token="";

    SearchAutoCompleteAdapter autoCompleteAdapter;


    boolean searchStatus = false;

    RelativeLayout bottomLayout;
    ArrayList<Product> productArrayList;

    AutoCompleteTextView searchBox;

    ListView spinner;

    SpinnerListAdapter spinnerListAdapter;
    ArrayList<SpinnerItem> spinnerItemArrayList;
    ImageButton searchBtn;
    LinearLayout drop;
    ImageButton cartBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);

        productArrayList = new ArrayList<>();
        cartBtn = (ImageButton) findViewById(R.id.cart);

        options = new ArrayList<>();
        options.add("Lowest Price");
        options.add("Highest Price");
        options.add("Name A to Z");
        options.add("Name Z to A");

        db = new DB(getApplicationContext());

         subCategoryId = getIntent().getStringExtra("subcategoryId");
        token = getIntent().getStringExtra("token");
        searchStatus = getIntent().getBooleanExtra("search",false);

        searchBox = (AutoCompleteTextView) findViewById(R.id.searchBox);
        productList= (GridView) findViewById(R.id.grid);
       sortBy = (TextView) findViewById(R.id.sort);
        filter = (TextView) findViewById(R.id.filter);
        title = (TextView) findViewById(R.id.title);
        searchBtn = (ImageButton) findViewById(R.id.searchBtn);

        drop = (LinearLayout) findViewById(R.id.drop);


        bottomLayout = (RelativeLayout) findViewById(R.id.bottom);
        spinner = (ListView) findViewById(R.id.spinner);


        autoCompleteProducts = new ArrayList<>();

        productListAdapter = new ProductListAdapter(ProductListActivity.this,productArrayList);
        productList.setAdapter(productListAdapter);



        AutoCompleteModel[] data = new AutoCompleteModel[0];
        autoCompleteAdapter = new SearchAutoCompleteAdapter(this, R.layout.auto_complete_list_item, data);
        searchBox.setAdapter(autoCompleteAdapter);


        searchBox.setHint("Search products");
        searchBox.setThreshold(2);

        setSpinner();


        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(bottomLayout.getVisibility()==View.VISIBLE){
                    bottomLayout.setVisibility(View.GONE);
                }else{
                    bottomLayout.setVisibility(View.VISIBLE);
                }
            }
        });

        productListAdapter.setCustomObjectListener(new ProductListAdapter.onItemClickListener() {
            @Override
            public void onItemClicked(int position, String token) {

                Log.e("token",token);
                if(token.equals("view")){


                    Log.e("id",productArrayList.get(position).getId()+"");

                    Intent it = new Intent(ProductListActivity.this,ProductDetailsActivity.class);
                    it.putExtra("productId",productArrayList.get(position).getId()+"");
                    startActivity(it);
                } else if(token.equals("cart")){
                    db.addProductInCart(productArrayList.get(position).getId()+"",
                            productArrayList.get(position).getTitle(),
                            productArrayList.get(position).getBrandid()+"",
                            productArrayList.get(position).getCategoryid()+"",
                            productArrayList.get(position).getCurrentprice()+"",
                            productArrayList.get(position).getOldprice()+"","5",
                            productArrayList.get(position).getMainimage(),
                            "1");

                    setCartCounter();

                    Toast.makeText(getApplicationContext(),"Added to "+token,Toast.LENGTH_LONG).show();


                }else if(token.equals("wish")){

                    if(!db.checkWishlistItem(productArrayList.get(position).getId()+"")){
                        db.addProductInWishlist(productArrayList.get(position).getId()+"",
                                productArrayList.get(position).getTitle(),
                                productArrayList.get(position).getBrandid()+"",
                                productArrayList.get(position).getCategoryid()+"",
                                productArrayList.get(position).getCurrentprice()+"",
                                productArrayList.get(position).getOldprice()+"","5",
                                productArrayList.get(position).getMainimage(),
                                "1");

                        Toast.makeText(ProductListActivity.this,"Added to "+token,Toast.LENGTH_LONG).show();

                    }
                    else{

                        db.removeFromWishlist(productArrayList.get(position).getId()+"");
                    }


                }
            }
        });


        pageNumber=1;

        if(token.equals("trending")){
            getProductsByType("trending");
            title.setText("Trending Products");

        }else if(token.equals("exclusive")){
            getProductsByType("exclusive");
            title.setText("Exclusive Products");
        }else if(searchStatus){
            searchBox.setText(token);
            bottomLayout.setVisibility(View.VISIBLE);
            term = token;
            getProducts();
        }else{
            term="0";
            getProducts();
            title.setText("Select product");
        }



        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProductListActivity.this,CartActivity.class);
                startActivity(intent);
            }
        });


        drop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  mPickerUI.setVisibility(View.VISIBLE);
              //  mPickerUI.slide();
                if(spinner.getVisibility()==View.VISIBLE) {

                    spinner.setVisibility(View.INVISIBLE);
                }else {

                    for (int i = 0; i < spinnerItemArrayList.size(); i++) {
                        if (spinnerItemArrayList.get(i).isSelected) {

                        //int listViewHeight = spinner.getMeasuredHeight();
                        // spinner.smoothScrollToPositionFromTop(i, (listViewHeight / 2));
                           spinner.smoothScrollToPosition(i);
                            spinner.setSelection(i);
                        }
                    }


                    spinner.setVisibility(View.VISIBLE);
                }


            }
        });

        sortBy.setText(options.get(0).toString());
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ProductListActivity.this,FilterActivity.class);


                startActivityForResult(i, RESPONSE_CODE_FILTER);
               // startActivity(i);
            }
        });











        productList.setOnScrollListener(new EndlessScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to your AdapterView


                if(productArrayList.size()>10*pageNumber) {
                    pageNumber++;

                    if (token.equals("trending")) {
                        getProductsByType("trending");
                    } else if (token.equals("exclusive")) {
                        getProductsByType("exclusive");
                    } else if(searchStatus){


                        getProducts();
                    }else{
                        term="0";
                        getProducts();

                    }
                }

            }
        });


//        mPickerUI.setOnClickItemPickerUIListener(new PickerUI.PickerUIItemClickListener() {
//            @Override
//            public void onItemClickPickerUI(int which, int position, String valueResult) {
//               sortBy.setText(options.get(position).toString());
//
//                if(position==0){
//                    sortkey=1;
//                }else if(position==1){
//                    sortkey=2;
//                }else if(position==2){
//                    sortkey=3;
//                }else if(position==3){
//                    sortkey=4;
//                }
//
//                getProducts();
//               // mPickerUI.setVisibility(View.GONE);
//            }
//        });



        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String temp = String.valueOf(s);
                term = temp;
                //Toast.makeText(context,search_item,Toast.LENGTH_SHORT).show();
                if (term.trim().length() > 0) {

                    term = searchBox.getText().toString();

                    searchItems();

                }
            }
        });


        searchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    pageNumber=1;
                    searchStatus = true;
                    term = searchBox.getText().toString();
                    productArrayList.clear();
                    productListAdapter.notifyDataSetChanged();
                    subCategoryId="0";
                    getProducts();

                    return true;
                }

                return false;
            }
        });


        searchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.e("id",autoCompleteProducts.get(position).getId()+"");


                searchBox.setText("");
                Intent it = new Intent(getApplicationContext(),ProductDetailsActivity.class);
                it.putExtra("productId",autoCompleteProducts.get(position).getId()+"");
                startActivity(it);

            }
        });


        ImageButton backBtn = (ImageButton) findViewById(menu);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    public void setSpinner(){
        spinnerItemArrayList = new ArrayList<>();

        for(int i=0;i<options.size();i++){
            Log.e("optionss",options.get(i).toString());
            SpinnerItem spinnerItem = new SpinnerItem(options.get(i).toString(),false);
            spinnerItemArrayList.add(spinnerItem);
        }

        spinnerItemArrayList.get(0).setSelected(true);




        spinnerListAdapter = new SpinnerListAdapter(getApplicationContext(),spinnerItemArrayList);
        spinner.setAdapter(spinnerListAdapter);



        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                for(int i=0;i<spinnerItemArrayList.size();i++){
                    if(i==position){
                        spinnerItemArrayList.get(i).setSelected(true);
                    }else {
                        spinnerItemArrayList.get(i).setSelected( false);
                    }
                }


                productArrayList.clear();

                spinnerListAdapter = new SpinnerListAdapter(getApplicationContext(),spinnerItemArrayList);
                spinner.setAdapter(spinnerListAdapter);


                spinner.setVisibility(View.GONE);


                sortBy.setText(spinnerItemArrayList.get(position).getTitle());

                if(position==0){
                    sortkey=1;
                }else if(position==1){
                    sortkey=2;
                }else if(position==2){
                    sortkey=3;
                }else if(position==3){
                    sortkey=4;
                }


                if(token.equals("trending")){
                    getProductsByType("trending");


                }else if(token.equals("exclusive")){
                    getProductsByType("exclusive");

                }else if(searchStatus){
                    searchBox.setText(token);
                    bottomLayout.setVisibility(View.VISIBLE);
                    term = token;
                    getProducts();
                }else{
                    term="0";
                    getProducts();

                }

            }
        });


    }



    public void getProducts(){



        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);


        Log.e("subCategoryId",subCategoryId);
        Log.e("brandId",brandId);
        Log.e("price1",price1);
        Log.e("price2",price2);
        Log.e("sortkey",sortkey+"");
        Log.e("term",term);
        Log.e("pageNumber",pageNumber+"");



        Call<ProductFilterResponse> call = apiService.getProducts(Integer.parseInt(subCategoryId),
                Integer.parseInt(brandId),

                 price1 ,
                 price2 ,
                 sortkey, term,
                pageNumber+"");


        call.enqueue(new Callback<ProductFilterResponse>() {
            @Override
            public void onResponse(Call<ProductFilterResponse>call, Response<ProductFilterResponse> response) {


                try {

                    boolean error = response.body().getError();

                    String msg = response.body().getMessage();



                    if (!error) {

                        Log.e("size",response.body().getData().getData().size()+"");
                        int s =response.body().getData().getData().size();

                        if(s!=0) {
                            for (int i = 0; i < s; i++) {
                                Log.e("i",i+"");
                                productArrayList.add(response.body().getData().getData().get(i));
                            }

                            productListAdapter.notifyDataSetChanged();
                        }else{
                            Toast.makeText(getApplicationContext(),"No More ProductDetails",Toast.LENGTH_LONG).show();
                        }

                    } else {

                    }


                }catch (Exception e){

                    e.printStackTrace();


                    Toast.makeText(getApplicationContext(),"No More Product",Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<ProductFilterResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());


            }
        });


    }



    public void getProductsByType(String type){



        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);


        Log.e("subCategoryId",subCategoryId);
        Log.e("brandId",brandId);
        Log.e("price1",price1);
        Log.e("price2",price2);
        Log.e("sortkey",sortkey+"");
        Log.e("term",term);
        Log.e("pageNumber",pageNumber+"");
        Log.e("type",type+"");



        Call<ProductFilterResponse> call = apiService.getProductsByTypes(type,Integer.parseInt(subCategoryId),
                Integer.parseInt(brandId),

                price1 ,
                price2 ,
                sortkey, term,
                pageNumber+"");


        call.enqueue(new Callback<ProductFilterResponse>() {
            @Override
            public void onResponse(Call<ProductFilterResponse>call, Response<ProductFilterResponse> response) {


                try {

                    boolean error = response.body().getError();

                    String msg = response.body().getMessage();



                    if (!error) {

                        Log.e("size",response.body().getData().getData().size()+"");
                        int s =response.body().getData().getData().size();

                        if(s!=0) {
                            for (int i = 0; i < s; i++) {
                                Log.e("i",i+"");
                                productArrayList.add(response.body().getData().getData().get(i));
                            }

                            productListAdapter.notifyDataSetChanged();
                        }else{
                            Toast.makeText(getApplicationContext(),"No More ProductDetails",Toast.LENGTH_LONG).show();
                        }

                    } else {

                    }


                }catch (Exception e){

                    e.printStackTrace();


                    Toast.makeText(getApplicationContext(),"No More ProductDetails",Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<ProductFilterResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());


            }
        });


    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==RESPONSE_CODE_FILTER)
        {
            try {
                //String message=data.getStringExtra("location");
                price1 = data.getStringExtra("price1");
                price2 = data.getStringExtra("price2");
                subCategoryId = data.getStringExtra("subcategoryId");
                brandId = data.getStringExtra("brandId");

                Log.e("price1",price1);
                Log.e("price2",price2);

                pageNumber =1;
                productArrayList.clear();
                productListAdapter.notifyDataSetChanged();
                getProducts();

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }


    public void searchItems(){




        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);





        Call<AutoCompleteResponse> call = apiService.getAutoComplete(term);



        call.enqueue(new Callback<AutoCompleteResponse>() {
            @Override
            public void onResponse(Call<AutoCompleteResponse> call, Response<AutoCompleteResponse> response) {


                try {


                    boolean error = response.body().getError();


                    if (!error) {


                        autoCompleteProducts = response.body().getData();

                        setAutoCompleteData();
                    } else {

                    }


                }catch (Exception e){

                    e.printStackTrace();


                    //     Toast.makeText(getApplicationContext(),"No More ProductDetails",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AutoCompleteResponse> call, Throwable t) {

            }
        });


    }



    public void setCartCounter(){


        TextView cartCounter = (TextView) findViewById(R.id.cartCounter);


        cartCounter.setText(db.getCartCount());
    }

    public void setAutoCompleteData(){

        objectItemData = new AutoCompleteModel[autoCompleteProducts.size()];

        for(int i=0;i<autoCompleteProducts.size();i++) {
            AutoCompleteModel autoItem = new AutoCompleteModel(autoCompleteProducts.get(i).getId()+""
                    , autoCompleteProducts.get(i).getTitle());
            objectItemData[i] = autoItem;
        }

        autoCompleteAdapter.notifyDataSetChanged();
        autoCompleteAdapter = new SearchAutoCompleteAdapter(getApplicationContext(), R.layout.auto_complete_list_item, objectItemData);
        searchBox.setAdapter(autoCompleteAdapter);

    }
}

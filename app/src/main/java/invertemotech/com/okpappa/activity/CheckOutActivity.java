package invertemotech.com.okpappa.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;

import java.util.Arrays;
import java.util.List;

import invertemotech.com.okpappa.R;

import static invertemotech.com.okpappa.R.id.menu;

public class CheckOutActivity extends AppCompatActivity {

    TextView areaTxt;
    PickerUI mPickerUI;

    List<String> options;

    int cuurentPosition=0;



    final String[] arr1=new String[]{"Chawkbazar","Rahmatganj","Agrabaad","LoveLane"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);


        mPickerUI = (PickerUI) findViewById(R.id.picker_ui_view);

        options = Arrays.asList(arr1);

        PickerUISettings pickerUISettings = new PickerUISettings.Builder()
                .withItems(options)

                .withAutoDismiss(true)
                .withItemsClickables(false)
                .withUseBlur(false)
                .build();

        mPickerUI.setSettings(pickerUISettings);





        mPickerUI.setOnClickItemPickerUIListener(new PickerUI.PickerUIItemClickListener() {
            @Override
            public void onItemClickPickerUI(int which, int position, String valueResult) {

                cuurentPosition = position;
                areaTxt.setText(options.get(position).toString());
                mPickerUI.slide(null);

            }
        });
        
        areaTxt = (TextView) findViewById(R.id.area);


        areaTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mPickerUI.slide(cuurentPosition);

            }
        });


        ImageButton backBtn = (ImageButton) findViewById(menu);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

package invertemotech.com.okpappa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.shawnlin.numberpicker.NumberPicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.adapter.CartListAdapter;
import invertemotech.com.okpappa.adapter.WishListAdapter;
import invertemotech.com.okpappa.adapter.WishListAdapterNew;
import invertemotech.com.okpappa.model.CartProductModel;
import invertemotech.com.okpappa.model.ProductsModel;
import invertemotech.com.okpappa.util.DB;

import static invertemotech.com.okpappa.R.id.menu;

public class WishListActivity extends AppCompatActivity {
    ArrayList<CartProductModel> productsModelArrayList;

    WishListAdapterNew wishListAdapter;
    final String[] arr1=new String[]{"1","2","3","4","5"};
    ListView list;
    PickerUI mPickerUI;
    List<String> options;
    NumberPicker horizontal_number_picker;
    int cuurentPosition;
    Button addQuantity;
    Button checkoutBtn;
    LinearLayout pickerLin;
    DB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        list = (ListView) findViewById(R.id.list);
        horizontal_number_picker = (NumberPicker) findViewById(R.id.horizontal_number_picker);
        addQuantity = (Button) findViewById(R.id.addQuantity);
        pickerLin = (LinearLayout) findViewById(R.id.pickerLin);
        mPickerUI = (PickerUI) findViewById(R.id.picker_ui_view);

        options = Arrays.asList(arr1);

        checkoutBtn = (Button) findViewById(R.id.checkOut);


        db =new DB(getApplicationContext());



        setList();

        ImageButton backBtn = (ImageButton) findViewById(R.id.bckBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void setList(){

        productsModelArrayList = new ArrayList<>();
        productsModelArrayList = db.getCartProducts();

        wishListAdapter = new WishListAdapterNew(WishListActivity.this,productsModelArrayList);
        list.setAdapter(wishListAdapter);



        wishListAdapter.setCustomObjectListener(new WishListAdapterNew.onItemClickListener() {
            @Override
            public void onItemClicked(int position, String token) {
                if(token.equals("quantity")){
                    pickerLin.setVisibility(View.VISIBLE);

                    String q=  productsModelArrayList.get(position).getProQuantity();

                    cuurentPosition = position;

                }else if(token.equals("remove")){

                    db.removeFromCart(productsModelArrayList.get(position).getProID());
//                     productsModelArrayList.clear();
//                    productsModelArrayList = db.getCartProducts();

                    setList();

                }else if(token.equals("view")){



                    Intent it = new Intent(WishListActivity.this,ProductDetailsActivity.class);
                    it.putExtra("productId",productsModelArrayList.get(cuurentPosition).getProID()+"");
                    startActivity(it);

                }else if(token.equals("cart")){
                    db.addProductInCart(productsModelArrayList.get(position).getProID()+"",
                            productsModelArrayList.get(position).getProName(),
                            productsModelArrayList.get(position).getProBrand()+"",
                            productsModelArrayList.get(position).getProCategory()+"",
                            productsModelArrayList.get(position).getProPriceNow()+"",
                            productsModelArrayList.get(position).getProPricePrevious()+"","5",
                            productsModelArrayList.get(position).getProImage(),
                            "1");

                  //  setCartCounter();

                    Toast.makeText(getApplicationContext(),"Added to "+token,Toast.LENGTH_LONG).show();


                }
            }
        });



        addQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int  productQuantity= horizontal_number_picker.getValue();


                productsModelArrayList.get(cuurentPosition).setProQuantity(productQuantity+"");
                wishListAdapter.notifyDataSetChanged();
                pickerLin.setVisibility(View.GONE);

                db.updateQuantityWishlist(productsModelArrayList.get(cuurentPosition).getProID(),productQuantity+"");


            }
        });

    }
}

package invertemotech.com.okpappa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;


import org.parceler.Parcels;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.retrofit.ApiClient;
import invertemotech.com.okpappa.retrofit.ApiInterafce;
import invertemotech.com.okpappa.retrofitmodel.SplashData;
import invertemotech.com.okpappa.retrofitmodel.SplashResponse;
import invertemotech.com.okpappa.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {

    SharePref sharePref;
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharePref = new SharePref(getApplicationContext());




        mImageView = (ImageView) findViewById(R.id.image);

//
//        final Animation zoomAnimation = AnimationUtils.loadAnimation(getBaseContext(), R.anim.zoom_out);
//        mImageView.startAnimation(zoomAnimation);
//        zoomAnimation.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                Handler mHandler = new Handler(getMainLooper());
//                Runnable mRunnable = new Runnable() {
//                    @Override
//                    public void run() {
//
//                        getMainUrl();
//                    }
//                };
//                mHandler.postDelayed(mRunnable, 3000);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });


        getMainUrl();
    }



    public void getMainUrl(){


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);




        Call<SplashResponse> call = apiService.getSplashData();


        call.enqueue(new Callback<SplashResponse>() {
            @Override
            public void onResponse(Call<SplashResponse>call, Response<SplashResponse> response) {



                try {


                    Log.e("suc", response.isSuccessful()+"");
                    Log.e("SPlash",response.body().getData().getBanners().size()+"");



                    SplashData splashData = new SplashData();
                    splashData = response.body().getData();

                    Log.e("SPlash",response.body().getData().getBanners().get(0).getCreatedAt());


                    Intent i = new Intent(SplashActivity.this,MainActivity.class);
                    Parcelable listParcelable = Parcels.wrap(splashData);
                    i.putExtra("splashdata", listParcelable);
                    startActivity(i);

                    finish();
                }catch (Exception e){

                    e.printStackTrace();


                }



            }

            @Override
            public void onFailure(Call<SplashResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());


            }
        });


    }

}







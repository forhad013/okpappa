package invertemotech.com.okpappa.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dpizarro.uipicker.library.picker.PickerUI;
import com.dpizarro.uipicker.library.picker.PickerUISettings;
import com.shawnlin.numberpicker.NumberPicker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import invertemotech.com.okpappa.R;
import invertemotech.com.okpappa.adapter.ProductImageAdapter;
import invertemotech.com.okpappa.adapter.ProductListAdapter;
import invertemotech.com.okpappa.adapter.ProductListAdapterForDetails;
import invertemotech.com.okpappa.model.ImageModel;
import invertemotech.com.okpappa.model.ProductsModel;
import invertemotech.com.okpappa.retrofit.ApiClient;
import invertemotech.com.okpappa.retrofit.ApiInterafce;
import invertemotech.com.okpappa.retrofitmodel.Product;
import invertemotech.com.okpappa.retrofitmodel.ProductDetails;
import invertemotech.com.okpappa.retrofitmodel.ProductDetailsResponse;
import invertemotech.com.okpappa.retrofitmodel.SimilarProducts;
import invertemotech.com.okpappa.util.DB;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static invertemotech.com.okpappa.R.id.menu;
import static invertemotech.com.okpappa.R.id.quantity;

public class ProductDetailsActivity extends AppCompatActivity {

    ProductListAdapterForDetails productListAdapterForDetails;
    ArrayList<ProductsModel> productsModelArrayList;
    ImageView  imageView;
    RecyclerView imagelist;
    RecyclerView productlist;

    TextView previousPrice;

    final String[] arr1=new String[]{"1","2","3","4","5"};

    PickerUI mPickerUI;
    List<String> options;

    TextView quantity;

    String productId;
    Button addQuantity;
    ArrayList<SimilarProducts> similarProducts;

    ImageButton cartBtn;

    ProductDetails productDetails;

    TextView title,available,priceNow,pricePrevious,details;

    RatingBar ratingBar;
    int productQuantity=1;
    Button addToCart,addToWishlist;

    LinearLayout pickerLin;

    DB db ;
    NumberPicker horizontal_number_picker;

    ArrayList<Integer> quantityList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        productId = getIntent().getStringExtra("productId");

        Log.e("pr",productId);

        horizontal_number_picker = (NumberPicker) findViewById(R.id.horizontal_number_picker);
        db = new DB(getApplicationContext());

        previousPrice = (TextView) findViewById(R.id.pricePrevious);
        quantity = (TextView) findViewById(R.id.quantity);
        imagelist = (RecyclerView)  findViewById(R.id.imageList);
        productlist = (RecyclerView)  findViewById(R.id.productList);
        imageView = (ImageView)  findViewById(R.id.image);
        ratingBar = (RatingBar) findViewById(R.id.rating);
        addToCart = (Button) findViewById(R.id.addToCart);
        addToWishlist = (Button) findViewById(R.id.addToWishlist);
        addQuantity = (Button) findViewById(R.id.addQuantity);
        title = (TextView) findViewById(R.id.productTitle);
        available = (TextView) findViewById(R.id.available);
        priceNow = (TextView) findViewById(R.id.priceNow);

        details = (TextView) findViewById(R.id.details);

        pickerLin = (LinearLayout) findViewById(R.id.pickerLin);

        cartBtn = (ImageButton) findViewById(R.id.cart);
        similarProducts = new ArrayList<>();

        previousPrice.setPaintFlags(previousPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        ratingBar.setNumStars(5);
        ratingBar.setRating(3);
//        ratingBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        //setImages();

        //setProductList();

        getProductDetails();

        for(int i=0;i<50;i++){
            quantityList.add(i+1);
        }


        quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                pickerLin.setVisibility(View.VISIBLE);

              //  horizontal_number_picker.setValue(productQuantity);

            }
        });



        addQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               productQuantity= horizontal_number_picker.getValue();
               quantity.setText(productQuantity+"");
               pickerLin.setVisibility(View.GONE);
            }
        });


        options = Arrays.asList(arr1);




        if(db.checkWishlistItem(productId)){
            addToWishlist.setText("Wishlist product");
            addToWishlist.setBackgroundColor(Color.RED);
        }else{
            addToWishlist.setText("Add to wishlist");
            addToWishlist.setBackgroundColor(getResources().getColor(R.color.btn_color));
        }

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db.addProductInCart(productId,productDetails.getTitle(),productDetails.getBrandid()+"",productDetails.getCategoryid()+"",productDetails.getCurrentprice()+"",productDetails.getOldprice()+"","5",productDetails.getMainimage(),productQuantity+"");

                setCartCounter();

                Toast.makeText(getApplicationContext(),"Added to cart",Toast.LENGTH_LONG).show();
            }
        });


        addToWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!db.checkWishlistItem(productId)){
                    db.addProductInWishlist(productId,productDetails.getTitle(),productDetails.getBrandid()+"",productDetails.getCategoryid()+"",productDetails.getCurrentprice()+"",productDetails.getOldprice()+"","5",productDetails.getMainimage(),productQuantity+"");
                    addToWishlist.setBackgroundColor(Color.RED);

                    addToWishlist.setText("Wishlist product");
                    addToWishlist.setBackgroundColor(Color.RED);
                    Toast.makeText(getApplicationContext(),"Added to wishlist",Toast.LENGTH_LONG).show();

                }
                else{


                    db.removeFromWishlist(productId);
                    addToWishlist.setText("Add to wishlist");
                    addToWishlist.setBackgroundColor(getResources().getColor(R.color.btn_color));
                }
            }
        });
        setCartCounter();



        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ProductDetailsActivity.this,CartActivity.class);
                startActivity(intent);
            }
        });


    }



    public void setProductDetails(){
        setImages();

        previousPrice.setText(productDetails.getOldprice()+"");
         priceNow.setText(productDetails.getCurrentprice()+"");
        details.setText(productDetails.getLargetext());


       // ratingBar.setRating();


        Log.e("e",productDetails.getTitle());


        title.setText(productDetails.getTitle());

        if(productDetails.getStatus()!=0) {
            available.setText("Available");
        }else{
            available.setText("Not Available");
        }



    }

    public void setCartCounter(){


        TextView cartCounter = (TextView) findViewById(R.id.cartCounter);


        cartCounter.setText(db.getCartCount());
    }


    public void setImages(){
        final ArrayList<ImageModel> images = new ArrayList<>();

        ImageModel imageModel = new ImageModel("1",  productDetails.getMainimage());
        images.add(imageModel);



        Picasso.with(ProductDetailsActivity.this).load(images.get(0).getUrl()).into(imageView);

        ProductImageAdapter productImageAdapter = new ProductImageAdapter(images,ProductDetailsActivity.this);
        imagelist.setAdapter( productImageAdapter);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(ProductDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);


        imagelist.setLayoutManager(layoutManager);


        productImageAdapter.setCustomObjectListener(new ProductImageAdapter.onItemClickListener() {
            @Override
            public void onItemClicked(int position) {

                String url = images.get(position).getUrl();

                Log.e("i",url);

                Picasso.with(ProductDetailsActivity.this).load(url).into(imageView);
            }
        });
    }



    public void setProductList(){




        productListAdapterForDetails = new ProductListAdapterForDetails(similarProducts,ProductDetailsActivity.this);
        productlist.setAdapter(productListAdapterForDetails);


        LinearLayoutManager layoutManager
                = new LinearLayoutManager(ProductDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false);


        productlist.setLayoutManager(layoutManager);



        productListAdapterForDetails.setCustomObjectListener(new ProductListAdapterForDetails.onItemClickListener() {
            @Override
            public void onItemClicked(int position, String token) {
                Log.e("token",token);
                if(token.equals("view")){


                    Log.e("id",similarProducts.get(position).getId()+"");

                    Intent it = new Intent(getApplicationContext(),ProductDetailsActivity.class);
                    it.putExtra("productId",similarProducts.get(position).getId()+"");
                    startActivity(it);
                }else if(token.equals("cart")){

                    db.addProductInCart(similarProducts.get(position).getId()+"",
                            similarProducts.get(position).getTitle(),
                            similarProducts.get(position).getBrandid()+"",
                            similarProducts.get(position).getCategoryid()+"",
                            similarProducts.get(position).getCurrentprice()+"",
                            similarProducts.get(position).getOldprice()+"","5",
                            similarProducts.get(position).getMainimage(),
                            1+"");

                    setCartCounter();
                }
                else if(token.equals("wish")){
                    if(!db.checkWishlistItem(similarProducts.get(position).getId()+"")){
                        db.addProductInWishlist(similarProducts.get(position).getId()+"",
                                similarProducts.get(position).getTitle(),
                                similarProducts.get(position).getBrandid()+"",
                                similarProducts.get(position).getCategoryid()+"",
                                similarProducts.get(position).getCurrentprice()+"",
                                similarProducts.get(position).getOldprice()+"","5",
                                similarProducts.get(position).getMainimage(),
                                "1");

                        Toast.makeText(getApplicationContext(),"Added to "+token,Toast.LENGTH_LONG).show();

                    }
                    else{

                        db.removeFromWishlist(similarProducts.get(position).getId()+"");

                    }

                }
            }
        });


        ImageButton backBtn = (ImageButton) findViewById(menu);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    public void getProductDetails(){



        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);


        Call<ProductDetailsResponse> call = apiService.getProductDetails(Integer.parseInt(productId));


        call.enqueue(new Callback<ProductDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductDetailsResponse>call, Response<ProductDetailsResponse> response) {


                try {

                    boolean error = response.body().getError();

                    String msg = response.body().getMessage();

                    Log.e("error",msg);


                    if (!error) {

                        productDetails = new ProductDetails();
                        productDetails = response.body().getData().getProduct();
                        similarProducts = response.body().getData().getSimilar();

                        setProductDetails();
                        setProductList();

                    } else {

                    }


                }catch (Exception e){

                    e.printStackTrace();


                    Toast.makeText(getApplicationContext(),"No More ProductDetails",Toast.LENGTH_LONG).show();
                }



            }

            @Override
            public void onFailure(Call<ProductDetailsResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());


            }
        });


    }
}
